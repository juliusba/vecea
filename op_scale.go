package vecea

import (
	"fmt"
	"math"
	"math/rand"
	"vecmat"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ DeltaDist --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

// OperatorDeltaDist sets the expected stepSize of the mutation to be the distance to the parents closest neighboor.
type OperatorDeltaDist struct {
	Tau float64
}

func (op *OperatorDeltaDist) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	deltaDist := original.meta["deltaDist"].(float64)
	sd := math.Abs(deltaDist + deltaDist*rand.NormFloat64()/2)
	mutLen := vecmat.ManhattanLen(mutVec)
	factor := sd / mutLen
	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			mutVec[i] *= factor
		}
	}
	// fmt.Println(vecmat.ManhattanLen(mutVec), sd)

	child.meta["deltaDist"] = sd
	// original.AddSurvivalCallback(func(child *Individ) {
	// 	child.meta["deltaDist"] = deltaDist + (sd-deltaDist)/op.Tau
	// })

	return false
}

func (op *OperatorDeltaDist) Init(ea *EA) {

	for _, individ := range ea.Population {
		deltaDist := 0.25 * individ.blueprint[0].Range * float64(len(individ.blueprint))
		individ.meta["deltaDist"] = deltaDist
	}
}

func (op *OperatorDeltaDist) String() string {
	return fmt.Sprintf("(Operator)\t DeltaDist")
}

func NewOperatorDeltaDist(tau float64) *OperatorDeltaDist {
	deltaDistOp := new(OperatorDeltaDist)
	deltaDistOp.Tau = tau
	return deltaDistOp
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------- MinDist --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

// OperatorMinDist sets the expected stepSize of the mutation to be the distance to the parents closest neighboor.
type OperatorMinDist struct{}

func (op *OperatorMinDist) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	minDist := math.MaxFloat64
	var basinFactor float64
	for i, individ := range ea.Population {
		if individ == original {
			basinFactor = float64(len(ea.Population)-i) / float64(len(ea.Population))
		} else if dist := original.DistanceTo(individ); dist < minDist {
			minDist = dist
		}
	}

	mutLen := vecmat.Len(mutVec)
	if minDist > mutLen {
		factor := minDist / mutLen
		factor -= (factor - 1) * basinFactor

		if deltaVec, ok := original.meta["deltaVec"].([]float64); ok {
			for i := 0; i < len(mutVec); i++ {
				deltaVec[i] *= factor
			}
		}

		for i := 0; i < len(mutVec); i++ {
			if mutVec[i] != 0 {
				mutVec[i] *= factor
			}
		}
	}
	return false
}

func (op *OperatorMinDist) String() string {
	return fmt.Sprintf("(Operator)\t MinDist")
}

func NewOperatorMinDist() *OperatorMinDist {
	minDistOp := new(OperatorMinDist)
	return minDistOp
}

// -------------------------------------------------------------------------------------------------------
// --------------------------------- Distance to average point -------------------------------------------
// -------------------------------------------------------------------------------------------------------

// OperatorDistToAvg sets the expected stepSize of the mutation to be the distance to the average point of
// the population
type OperatorDistToAvg struct{}

func (op *OperatorDistToAvg) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	minDist := math.MaxFloat64
	var basinFactor float64
	for i, individ := range ea.Population {
		if individ == original {
			basinFactor = float64(len(ea.Population)-i) / float64(len(ea.Population))
		} else if dist := original.DistanceTo(individ); dist < minDist {
			minDist = dist
		}
	}

	mutLen := vecmat.Len(mutVec)
	if minDist > mutLen {
		factor := minDist / mutLen
		factor -= (factor - 1) * basinFactor

		if deltaVec, ok := original.meta["deltaVec"].([]float64); ok {
			for i := 0; i < len(mutVec); i++ {
				deltaVec[i] *= factor
			}
		}

		for i := 0; i < len(mutVec); i++ {
			if mutVec[i] != 0 {
				mutVec[i] *= factor
			}
		}
	}
	return false
}

func (op *OperatorDistToAvg) String() string {
	return fmt.Sprintf("(Operator)\t DistToAvg")
}

func NewOperatorDistToAvg() *OperatorDistToAvg {
	minDistOp := new(OperatorDistToAvg)
	return minDistOp
}
