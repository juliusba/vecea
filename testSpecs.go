package vecea

import (
	"fmt"
	"log"
	"math"
)

type TestType int

const (
	TestType_Exponential TestType = iota
	TestType_Logarithmic TestType = iota
	TestType_Flat        TestType = iota
)

type TestSpecs struct {
	Type       TestType
	Modularity float64
	Locality   float64

	Min  float64
	Max  float64
	Mean float64

	Resolution int
}

func GetTestSpecs(tester interface{}, resolution int, blueprinter Blueprinter) (specs TestSpecs) {
	specs.Resolution = resolution
	bp := blueprinter.Blueprint()
	dimensionResolution := math.Pow(float64(resolution), 1.0/float64(len(bp)))
	fmt.Println(dimensionResolution)
	individ := NewIndivid(blueprinter.Blueprint())
	var score float64
	var tests int
	percentTests := resolution / 100
	percentDone := 0
	fn := func(individ *Individ) {
		solution := blueprinter.Phenotype(individ)
		if individTester, ok := tester.(IndividTester); ok {
			score = individTester.TestIndivid(individ, solution)
		} else if solutionTester, ok := tester.(SolutionTester); ok {
			score = solutionTester.TestSolution(solution)
		}
		if math.IsNaN(score) {
			log.Fatal("Score is nan, shouldnet even get here", individ.genome)
		}

		specs.Min = math.Min(specs.Min, score)
		specs.Max = math.Max(specs.Max, score)
		specs.Mean += score
		tests++
		if tests%percentTests == 0 {
			percentDone++
			fmt.Printf("\rProgress: %v, Min: %v, Max %v, Mean: %v", percentDone, specs.Min, specs.Max, specs.Mean)
		}
	}

	for i, el := range bp {
		individ.genome[i] = el.Min
	}
	index := 0
loop:
	newVal := individ.genome[index] + bp[index].Range/dimensionResolution
	if newVal > bp[index].Max {
		if index == 0 {
			goto end
		}
		individ.genome[index] = bp[index].Min
		index--
		goto loop
	}
	fn(individ)
	individ.genome[index] = newVal
	if index < len(individ.genome)-1 {
		index++
		goto loop
	} else {
		goto loop
	}
end:

	fmt.Println("\r         ")
	specs.Mean /= float64(resolution)
	meanPlacement := (specs.Mean - specs.Min) / (specs.Max - specs.Min)
	if meanPlacement > 0.9 {
		specs.Type = TestType_Logarithmic
	} else if meanPlacement < 0.1 {
		specs.Type = TestType_Exponential
	} else {
		specs.Type = TestType_Flat
	}

	return
}
