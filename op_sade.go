package vecea

import (
	"fmt"
	"math/rand"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------ Self-Adaptive SADE ---------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorSADE struct {
	FchangeRate float64
	Fmin, Fmax  float64

	CRchangeRate float64
	CRmin, CRmax float64
}

func (op *OperatorSADE) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {

	F, CR := op.getControlParameters(parents[0])
	child.SetMeta("sadeF", F, false)
	child.SetMeta("sadeCR", CR, false)

	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < CR {
			mutVec[i] = F * (parents[1].genome[i] - parents[2].genome[i])
			crossVec[i] = 3
		}
	}
	child.SetMeta("parent", parents[0], false)

	return false
}

func (op *OperatorSADE) getControlParameters(original *Individ) (F, CR float64) {
	if rand.Float64() < op.FchangeRate {
		F = op.Fmin + rand.Float64()*(op.Fmax-op.Fmin)
	} else {
		var ok bool
		F, ok = original.meta["sadeF"].(float64)
		if !ok {
			F = op.Fmin + rand.Float64()*(op.Fmax-op.Fmin)
		}
	}

	if rand.Float64() < op.CRchangeRate {
		CR = op.CRmin + rand.Float64()*(op.CRmax-op.CRmin)
	} else {
		var ok bool
		CR, ok = original.meta["sadeCR"].(float64)
		if !ok {
			CR = op.CRmin + rand.Float64()*(op.CRmax-op.CRmin)
		}
	}

	return
}

func (op *OperatorSADE) ParentDemand() int {
	return 4
}

// func (op *OperatorSADE) Init(ea *EA) {
// 	for i := range ea.Population {
// 		ea.Population[i].meta["sadeF"] = op.Fmin + rand.Float64()*(op.Fmax-op.Fmin)
// 		ea.Population[i].meta["sadeCR"] = op.CRmin + rand.Float64()*(op.CRmax-op.CRmin)
// 	}
// }

func (op *OperatorSADE) String() string {
	return fmt.Sprintf("(Operator)\t SADE (CRchangeRate(changeRate-Min-Max): %v-%v-%v, F(changeRate-Min-Max): %v-%v-%v)",
		op.CRchangeRate, op.CRmin, op.CRmax, op.FchangeRate, op.Fmax, op.Fmin)
}

func NewOperatorSADE(FchangeRate, Fmin, Fmax, CRchangeRate, CRmin, CRmax float64) *OperatorSADE {
	op := new(OperatorSADE)
	op.FchangeRate = FchangeRate
	op.Fmin = Fmin
	op.Fmax = Fmax
	op.CRchangeRate = CRchangeRate
	op.CRmin = CRmin
	op.CRmax = CRmax
	return op
}
