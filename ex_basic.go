package vecea

import (
	"fmt"
	"log"
)

type Executioner interface {
	Execute(*EA)
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Standard --------------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerStandard struct {
	adultCount int
}

func (ex *ExecutionerStandard) Execute(ea *EA) {
	ea.Population.Sort()

	for i := ex.adultCount; i < len(ea.Population); i++ {
		ea.Population[i].Die()
	}
	ea.Population = ea.Population[:ex.adultCount]
}

func (ex *ExecutionerStandard) Init(ea *EA) {
	if ex.adultCount <= 0 {
		ex.adultCount = ea.generationSize
	}
}

func (ex *ExecutionerStandard) String() string {
	return fmt.Sprintf("(Executioner)\t Standard (adultCount: %v)\n", ex.adultCount)
}

func NewExecutionerStandard(adultCount int) *ExecutionerStandard {
	executioner := new(ExecutionerStandard)
	executioner.adultCount = adultCount

	return executioner
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Oedipus ---------------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerOedipus struct{}

var (
	lol, lolTotal float64
)

func (ex *ExecutionerOedipus) Execute(ea *EA) {
	for _, child := range ea.children {
		if ea.IndexOfIndivid(child) == -1 {
			log.Fatal("Why here?")
			continue
		}
		var candidate *Individ
		if parent, ok := child.meta["parent"].(*Individ); ok {
			candidate = parent
		} else {
			candidate = child.Parent()
		}

		index := ea.IndexOfIndivid(candidate)
		if index == -1 {
			index = child.closestNeighboor(ea.Population)
			candidate = ea.Population[index]
		}

		lolTotal++
		if child.fitness < candidate.fitness {
			candidate.Die()
			lol++
		} else {
			index = ea.IndexOfIndivid(child)
			child.Die()
		}

		ea.Population[index], ea.Population = ea.Population[len(ea.Population)-1], ea.Population[:len(ea.Population)-1]
	}
	temp := make(Population, len(ea.Population))
	copy(temp, ea.Population)
	ea.Population = temp
}

func (ex *ExecutionerOedipus) String() string {
	return fmt.Sprintln("(Executioner)\t Oedipus")
}

func NewExecutionerOedipus() *ExecutionerOedipus {
	return new(ExecutionerOedipus)
}
