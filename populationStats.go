package vecea

import (
	"log"
	"math"
)

func (pop Population) Stats(fn func(*Individ) float64, sd bool) (stats Stats) {
	vals := make([]float64, len(pop))
	for i, individ := range pop {
		vals[i] = fn(individ)
	}
	stats = NewStats(vals, sd)
	return
}

func (pop Population) StatsArray(fn func(*Individ) []float64, sd bool) (stats []Stats) {
	// Mean, Min, Max
	l := len(fn(pop[0]))
	stats = make([]Stats, l)
	vals := make([][]float64, l)
	for i := range vals {
		vals[i] = make([]float64, len(pop))
	}
	for popIndex, individ := range pop {
		individVals := fn(individ)
		for i := range vals {
			vals[i][popIndex] = individVals[i]
		}
	}
	for i := range vals {
		stats[i] = NewStats(vals[i], sd)
	}
	return
}

func (pop Population) StatsFitness() (stats Stats) {
	vals := make([]float64, len(pop))
	for i, individ := range pop {
		vals[i] = individ.fitness
	}
	stats = NewStats(vals)
	return
}

func (pop Population) StatsTestScores() (stats map[string]Stats) {
	stats = make(map[string]Stats)
	for testName := range pop[0].testScores {
		vals := make([]float64, len(pop))
		for i, individ := range pop {
			vals[i] = individ.testScores[testName]
		}
		stats[testName] = NewStats(vals)
	}
	return
}

func (pop Population) StatsAge() (stats Stats) {
	vals := make([]float64, len(pop))
	for i, individ := range pop {
		vals[i] = float64(individ.age)
	}
	stats = NewStats(vals)
	return
}

func (pop Population) StatsSuccessRate() (stats Stats) {
	vals := make([]float64, 0, len(pop))
	for _, individ := range pop {
		if val := individ.ChildSuccessRate(); val != -1 {
			vals = append(vals, val)
		}
	}
	stats = NewStats(vals)
	return
}

func (pop Population) StatsGrandChildrenPerChild() (stats Stats) {
	vals := make([]float64, 0, len(pop))
	for _, individ := range pop {
		if val := individ.GrandChildrenPerChild(); val != -1 {
			vals = append(vals, val)
		}
	}
	stats = NewStats(vals)
	return
}

func (pop Population) StatsGenomeDistribution() (stats []Stats) {
	genomeLen := len(pop[0].genome)
	stats = make([]Stats, genomeLen)
	vals := make([]float64, len(pop))
	for g := 0; g < genomeLen; g++ {
		for i, individ := range pop {
			vals[i] = individ.genome[g]
		}
		stats[g] = NewStats(vals)
	}
	return
}

func (pop Population) StatsGeneUniqueness() (stats Stats) {
	genomeLen := len(pop[0].genome)
	vals := make([]float64, genomeLen)
	for g := 0; g < genomeLen; g++ {
		geneVals := make([]float64, 0, len(pop))
	individLoop:
		for _, individ := range pop {
			for _, val := range geneVals {
				if val == individ.genome[g] {
					continue individLoop
				}
			}
			geneVals = append(geneVals, individ.genome[g])
		}
		vals[g] = float64(len(geneVals)) / float64(len(pop))
	}
	stats = NewStats(vals)
	return
}

func (pop Population) StatsGeneSharing() (stats Stats) {
	genomeLen := len(pop[0].genome)
	vals := make([]float64, 0, int(math.Pow(float64(len(pop))-1, 2)/2))
	for i := 0; i < len(pop); i++ {
		for j := i + 1; j < len(pop); j++ {
			identicalCount := 0.0
			for g := 0; g < genomeLen; g++ {
				if pop[i].genome[g] == pop[j].genome[g] {
					identicalCount++
				}
			}
			if identicalCount/float64(genomeLen) > 1 {
				log.Fatal(identicalCount, genomeLen)
			}
			vals = append(vals, identicalCount/float64(genomeLen))
		}
	}
	stats = NewStats(vals)
	return
}

func (pop Population) StatsGeneSpread() (spreadStats, sdStats Stats) {
	bp := pop[0].blueprint
	genomeDistribution := pop.StatsGenomeDistribution()
	spreadVals := make([]float64, 0, len(genomeDistribution))
	sdVals := make([]float64, 0, len(genomeDistribution))
	for g, stats := range genomeDistribution {
		spreadVals = append(spreadVals, stats.Max-stats.Min/bp[g].Range)
		sdVals = append(sdVals, stats.SD/bp[g].Range)
	}
	spreadStats = NewStats(spreadVals)
	sdStats = NewStats(sdVals)
	return
}

func (pop Population) StatsDistance() (meanStats, minStats, maxStats Stats) {
	dists := make([][]float64, len(pop))
	for i := range dists {
		dists[i] = make([]float64, len(pop)-1)
	}
	unitVec := pop[0].blueprint.Ranges()
	for i, one := range pop {
		for j := i + 1; j < len(pop); j++ {
			other := pop[j]
			dist := one.DistanceManhattanUnitTo(other, unitVec)
			dists[i][j-1] = dist
			dists[j][i] = dist
		}
	}
	individStats := make([]Stats, len(dists))
	for i, vals := range dists {
		individStats[i] = NewStats(vals, false)
	}
	meanVals := make([]float64, len(individStats))
	minVals := make([]float64, len(individStats))
	maxVals := make([]float64, len(individStats))
	for i, stats := range individStats {
		minVals[i] = stats.Min
		meanVals[i] = stats.Mean
		maxVals[i] = stats.Max
	}
	meanStats = NewStats(meanVals)
	minStats = NewStats(minVals)
	maxStats = NewStats(maxVals)
	return
}

func (pop Population) StatsRelation() (meanStats, minStats, maxStats Stats) {
	rels := make([][]float64, len(pop))
	for i := range rels {
		rels[i] = make([]float64, len(pop)-1)
	}
	for i, one := range pop {
		for j := i + 1; j < len(pop); j++ {
			other := pop[j]
			rel := float64(one.RelationTo(other))
			rels[i][j-1] = rel
			rels[j][i] = rel
		}
	}
	individStats := make([]Stats, len(rels))
	for i, vals := range rels {
		individStats[i] = NewStats(vals, false)
	}
	meanVals := make([]float64, len(individStats))
	minVals := make([]float64, len(individStats))
	maxVals := make([]float64, len(individStats))
	for i, stats := range individStats {
		minVals[i] = stats.Min
		meanVals[i] = stats.Mean
		maxVals[i] = stats.Max
	}
	meanStats = NewStats(meanVals)
	minStats = NewStats(minVals)
	maxStats = NewStats(maxVals)
	return
}

func (ea *EA) StatsDistance() (meanStats, minStats, maxStats Stats) {
	if ea.distanceStats.lastUpdate < ea.generation {
		ea.distanceStats.mean, ea.distanceStats.min, ea.distanceStats.max = ea.Population.StatsDistance()
		ea.distanceStats.lastUpdate = ea.generation
	}
	meanStats, minStats, maxStats = ea.distanceStats.mean, ea.distanceStats.min, ea.distanceStats.max
	return
}

func (ea *EA) StatsMeanDistance() (stats Stats) {
	if ea.distanceStats.lastUpdate < ea.generation {
		ea.distanceStats.mean, ea.distanceStats.min, ea.distanceStats.max = ea.Population.StatsDistance()
		ea.distanceStats.lastUpdate = ea.generation
	}
	stats = ea.distanceStats.mean
	return
}

func (ea *EA) StatsMinDistance() (stats Stats) {
	if ea.distanceStats.lastUpdate < ea.generation {
		ea.distanceStats.mean, ea.distanceStats.min, ea.distanceStats.max = ea.Population.StatsDistance()
		ea.distanceStats.lastUpdate = ea.generation
	}
	stats = ea.distanceStats.min
	return
}

func (ea *EA) StatsMaxDistance() (stats Stats) {
	if ea.distanceStats.lastUpdate < ea.generation {
		ea.distanceStats.mean, ea.distanceStats.min, ea.distanceStats.max = ea.Population.StatsDistance()
		ea.distanceStats.lastUpdate = ea.generation
	}
	stats = ea.distanceStats.max
	return
}

type MetaStater interface {
	MetaStats(stats map[string]Stats)
}

func (ea *EA) StatsMeta() (stats map[string]Stats) {
	stats = make(map[string]Stats)
	for _, comp := range ea.Components {
		if stater, ok := comp.(MetaStater); ok {
			stater.MetaStats(stats)
		}
	}
	return
}

type MetaGenomeDistributioner interface {
	MetaGenomeDistribution(stats map[string][]Stats)
}

func (ea *EA) StatsGenomeDistributionMeta() (stats map[string][]Stats) {
	stats = make(map[string][]Stats)
	for _, comp := range ea.Components {
		if stater, ok := comp.(MetaGenomeDistributioner); ok {
			stater.MetaGenomeDistribution(stats)
		}
	}
	return
}
