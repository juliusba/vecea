package vecea

import (
	"fmt"
	"math"
	"math/rand"
)

type Operator interface {
	ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------------- Rate ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorRate struct {
	Rate float64
}

func (op *OperatorRate) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	mutCount := 0
	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < op.Rate {
			mutVec[i] = 1.0
			mutCount++
		}
	}
	for mutCount < 2 {
		mutVec[rand.Intn(len(mutVec))] = 1.0
		mutCount++
	}
	return false
}

func (op *OperatorRate) String() string {
	return fmt.Sprintf("(Operator)\t Rate (Rate: %v)", op.Rate)
}

func NewOperatorRate(rate float64) (op *OperatorRate) {
	op = new(OperatorRate)
	op.Rate = rate
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Rate NPoint ------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorRateNPoint struct {
	Rate float64
}

func (op *OperatorRateNPoint) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	rate := op.Rate
	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < rate {
			mutVec[i] = 1.0
			rate = math.Max(math.Min(op.Rate*4, 0.5), op.Rate)
		} else {
			rate = op.Rate
		}
	}

	return false
}

func (op *OperatorRateNPoint) String() string {
	return fmt.Sprintf("(Operator)\t Rate (Rate: %v)", op.Rate)
}

func NewOperatorRateNPoint(rate float64) (op *OperatorRateNPoint) {
	op = new(OperatorRateNPoint)
	op.Rate = rate
	return
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------------- Rand ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorRand struct{}

func (op *OperatorRand) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(mutVec); i++ {
		mutVec[i] *= rand.Float64()
	}
	return false
}

func (op *OperatorRand) String() string {
	return fmt.Sprintf("(Operator)\t Rand")
}

func NewOperatorRand() *OperatorRand {
	randOp := new(OperatorRand)
	return randOp
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------------- Gauss --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorGauss struct{}

func (op *OperatorGauss) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			temp := mutVec[i] * rand.NormFloat64()
			for temp == 0 {
				temp = mutVec[i] * rand.NormFloat64()
			}
			mutVec[i] = temp
		}
	}
	return false
}

func (op *OperatorGauss) String() string {
	return fmt.Sprintf("(Operator)\t Gauss")
}

func NewOperatorGauss() *OperatorGauss {
	gaussOp := new(OperatorGauss)
	return gaussOp
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- GaussMean --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorGaussMean struct {
	RatioMeanSD float64
}

func (op *OperatorGaussMean) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(mutVec); i++ {
		temp := mutVec[i] + mutVec[i]*rand.NormFloat64()
		for temp < 0.0 {
			temp = mutVec[i] + op.RatioMeanSD*mutVec[i]*rand.NormFloat64()
		}
		mutVec[i] = temp
		if rand.Float64() < 0.5 {
			mutVec[i] *= -1
		}
	}
	return false
}

func (op *OperatorGaussMean) String() string {
	return fmt.Sprintf("(Operator)\t GaussMean (RatioMeanSD: %v)", op.RatioMeanSD)
}

func NewOperatorGaussMean(ratioMeanSD float64) *OperatorGaussMean {
	op := new(OperatorGaussMean)
	op.RatioMeanSD = ratioMeanSD
	return op
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------------- Power --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorPower struct{}

func (op *OperatorPower) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			temp := 1.0 / (math.Pow(rand.Float64(), 1)) //1.0/mutVec[i]))
			mutVec[i] *= temp
		}
	}
	return false
}

func (op *OperatorPower) String() string {
	return fmt.Sprintf("(Operator)\t Power")
}

func NewOperatorPower() *OperatorPower {
	op := new(OperatorPower)
	return op
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- ScaleRange ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorScaleRange struct {
	RangePercentage float64
	TimeScaling     bool
	factor          float64
	t               float64
}

func (op *OperatorScaleRange) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			// Use sqrt because DivEA uses variance instead of sd. Temporary
			mutVec[i] *= math.Sqrt(op.factor * child.blueprint[i].Range)
		}
	}
	return false
}

func (opScale *OperatorScaleRange) Update(ea *EA) {
	if opScale.TimeScaling {
		opScale.t++
		opScale.factor = 1.0 / (math.Sqrt(opScale.t + 1))
	}
}

func (op *OperatorScaleRange) String() string {
	return fmt.Sprintf("(Operator)\t Scale (RangePercentage: %v, TimeScaling: %t)",
		op.RangePercentage, op.TimeScaling)
}

func NewOperatorScaleRange(rangePercentage float64, timeScaling bool) *OperatorScaleRange {
	op := new(OperatorScaleRange)
	op.RangePercentage = rangePercentage
	op.TimeScaling = timeScaling
	op.factor = op.RangePercentage
	return op
}
