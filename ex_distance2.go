package vecea

import (
	"fmt"
	"math"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Distance ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerDistance2 struct {
	Min       bool
	Manhattan bool
	Unit      bool

	Weight       float64
	SurvivalRate float64
	UnitVector   []float64

	distValFunc func(i1, i2 *Individ, distVal *float64)

	survivorCount float64
}

func (ex *ExecutionerDistance2) Execute(ea *EA) {
	ea.Population.Sort()
	var (
		contestors = ea.Population[1:]
		survivors  = make(Population, 1, int(ex.survivorCount+0.5))
		survivor   = ea.Population[0]
		maxDist    float64
	)

	survivors[0] = ea.Population[0]
	for _, contestor := range contestors {
		contestor.SetMeta("distVal", math.MaxFloat64, false)
	}

	for len(survivors) < cap(survivors) {
		var minFit = contestors[0].fitness
		for _, contestor := range contestors {
			minFit = math.Min(contestor.fitness, minFit)

			distVal := contestor.Meta("distVal").(float64)
			ex.distValFunc(contestor, survivor, &distVal)
			contestor.SetMeta("distVal", distVal, false)
			// maxDist = math.Max(maxDist, distVal)
		}

		var bestIndex int
		if maxDist == 0 {
			bestIndex = 0
		} else {
			bestVal := -math.MaxFloat64
			for i, contestor := range contestors {
				// fmt.Println(1-contestor.fitness/minFit,
				// 	ex.Weight*(contestor.Meta("distVal").(float64)/maxDist))
				multiVal := -contestor.fitness/minFit +
					ex.Weight*(contestor.Meta("distVal").(float64)/maxDist)
				if multiVal > bestVal {
					bestVal = multiVal
					bestIndex = i
				}
			}
		}
		survivor = contestors[bestIndex]
		survivors = append(survivors, survivor)
		contestors = append(contestors[:bestIndex], contestors[bestIndex+1:]...)
		if maxDist == 0 {
			maxDist = survivor.Meta("distVal").(float64)
		} else {
			maxDist = math.Max(maxDist, survivor.Meta("distVal").(float64))
		}
	}

	for _, nonSurvivor := range contestors {
		nonSurvivor.Die()
	}

	ea.Population = survivors
	if ex.Unit {
		distribution := survivors.StatsGenomeDistribution()
		for i, geneStats := range distribution {
			ex.UnitVector[i] = math.Max(geneStats.Max-geneStats.Min, contestors[0].blueprint[i].Range*math.Pow10(-5))
		}
	}
	// fmt.Println(ex.UnitVector)
	// time.Sleep(time.Second * 3)
}

func (ex *ExecutionerDistance2) Init(ea *EA) {
	if ex.survivorCount == 0 {
		if ex.SurvivalRate != 0 {
			ex.survivorCount = ex.SurvivalRate * float64(ea.generationSize)
		} else {
			ex.survivorCount = float64(ea.generationSize)
		}
	}

	ex.UnitVector = ea.Blueprint().Ranges()
}

func (ex *ExecutionerDistance2) String() string {
	return fmt.Sprintf("(Executioner)\t Distance2 (Min: %t, Manhattan: %t, Normalized: %t, Weight: %v, SurvivalRate: %v)",
		ex.Min, ex.Manhattan, ex.Unit, ex.Weight, ex.SurvivalRate)
}

func NewExecutionerDistance2(weight float64, survivalRate float64, min, manhattan, unit bool) (ex *ExecutionerDistance2) {
	ex = new(ExecutionerDistance2)
	ex.Weight = weight
	ex.SurvivalRate = survivalRate
	ex.Min = min
	ex.Manhattan = manhattan
	ex.Unit = unit

	var distFunc func(i1, i2 *Individ) float64
	if manhattan {
		distFunc = func(i1, i2 *Individ) float64 {
			return i1.DistanceManhattanUnitTo(i2, ex.UnitVector)
		}
	} else {
		distFunc = func(i1, i2 *Individ) float64 {
			return i1.DistanceUnitTo(i2, ex.UnitVector)
		}
	}

	if min {
		ex.distValFunc = func(subject, victor *Individ, distVal *float64) {
			dist := distFunc(subject, victor)
			if *distVal == 0 {
				subject.Die()
			}
			*distVal = math.Min(*distVal, dist)
		}
	} else {
		ex.distValFunc = func(subject, victor *Individ, distVal *float64) {
			dist := distFunc(subject, victor)
			if dist == 0 {
				subject.Die()
			}
			*distVal += dist
		}
	}

	return
}
