package vecea

import (
	"math"
	"math/rand"
	"vecmat"
)

type SurvivalListener interface {
	Survived(ind *Individ)
}

type DeathListener interface {
	IsDead(ind *Individ)
}

type Individ struct {
	genome    []float64
	barCode   []float64
	blueprint Blueprint
	Phenotype interface{}

	fitness float64
	expVal  float64

	birthGeneration    int
	siblingIndex       int
	age                int
	childCount         int
	childSurvivorCount int
	grandChildCount    int

	ancestorBound int
	ancestors     []*Individ

	meta       map[string]interface{}
	testScores map[string]float64

	survivalListeners []SurvivalListener
	survivalCallbacks []func(survivor *Individ)
	deathListeners    []DeathListener
}

// Getters
func (ind *Individ) Genome() []float64 {
	return ind.genome
}
func (ind *Individ) Fitness() float64 {
	return ind.fitness
}
func (ind *Individ) ExpVal() float64 {
	return ind.expVal
}
func (ind *Individ) Age() int {
	return ind.age
}
func (ind *Individ) Children() int {
	return ind.childCount
}
func (ind *Individ) GrandChildren() int {
	return ind.grandChildCount
}
func (ind *Individ) Ancestors() []*Individ {
	return ind.ancestors
}
func (ind *Individ) Meta(name string) interface{} {
	return ind.meta[name]
}
func (ind *Individ) TestScore(name string) float64 {
	return ind.testScores[name]
}

// Setters
func (ind *Individ) SetFitness(fitness float64) {
	ind.fitness = fitness
}
func (ind *Individ) IncAge() {
	ind.age++
	if ind.age == 1 {
		ind.Survive()
	}
}
func (ind *Individ) SetGenome(genome []float64) {
	ind.genome = genome
	for i := 0; i < len(ind.genome); i++ {
		ind.genome[i] = math.Min(math.Max(ind.blueprint[i].Min, ind.genome[i]), ind.blueprint[i].Max)
	}
}
func (child *Individ) SetParent(parent *Individ) {
	child.blueprint = parent.blueprint
	child.ancestorBound = parent.ancestorBound
	child.siblingIndex = parent.childCount
	if child.ancestorBound == 1 {
		child.ancestors = []*Individ{parent}
	} else if len(parent.ancestors) == parent.ancestorBound {
		child.ancestors = append(parent.ancestors[1:], parent)
	} else {
		child.ancestors = append(parent.ancestors, parent)
	}

	// Generate BarCode
	copy(child.barCode, parent.barCode)
	child.barCode[rand.Intn(len(child.barCode))] = rand.ExpFloat64()

	// Update childStats
	child.AddSurvivalListener(parent)
	parent.childCount++
	if grandParent := parent.Parent(); grandParent != nil {
		grandParent.grandChildCount++
	}
}
func (ind *Individ) SetMeta(name string, data interface{}, keepAfterDeath bool) {
	if data == nil {
		delete(ind.meta, name)
		return
	}
	ind.meta[name] = data
	if !keepAfterDeath {
		ind.AddDeathListener(metaCleaner(name))
	}
}

// Methods
func (ind *Individ) Parent() *Individ {
	if len(ind.ancestors) == 0 {
		return nil
	}
	return ind.ancestors[len(ind.ancestors)-1]
}
func (ind *Individ) ChildSuccessRate() float64 {
	if ind.childCount == 0 {
		return -1
	}
	return float64(ind.childSurvivorCount) / float64(ind.childCount)
}
func (ind *Individ) GrandChildrenPerChild() float64 {
	if ind.childCount == 0 {
		return -1
	}
	return float64(ind.grandChildCount) / float64(ind.childCount)
}
func (one *Individ) IsSibling(other *Individ) bool {
	return one.Parent() == other.Parent()
}
func (one *Individ) RelationTo(other *Individ) (relation int) {
	for i := range one.barCode {
		if one.barCode[i] != other.barCode[i] {
			relation++
		}
	}
	relation = (relation + 1) / 2
	return
}
func (ind *Individ) BoundGenome() {
	for i := 0; i < len(ind.genome); i++ {
		if ind.blueprint[i].Circular {
			if ind.genome[i] > ind.blueprint[i].Max {
				ind.genome[i] = math.Min(ind.blueprint[i].Min+ind.blueprint[i].Range*0.1, ind.genome[i]-ind.blueprint[i].Range)
			} else if ind.genome[i] < ind.blueprint[i].Min {
				ind.genome[i] = math.Max(ind.blueprint[i].Max-ind.blueprint[i].Range*0.1, ind.genome[i]+ind.blueprint[i].Range)
			}
		} else {
			ind.genome[i] = math.Min(math.Max(ind.blueprint[i].Min, ind.genome[i]), ind.blueprint[i].Max)
		}
	}
}
func (ind *Individ) SetAncestorBound(ancestorBound int) {
	ind.ancestorBound = ancestorBound
	if len(ind.ancestors) > ancestorBound {
		ind.ancestors = ind.ancestors[len(ind.ancestors)-ancestorBound:]
	}
}
func (ind *Individ) Randomize() {
	for i := 0; i < len(ind.genome); i++ {
		ind.genome[i] = ind.blueprint[i].Min + rand.Float64()*(ind.blueprint[i].Max-ind.blueprint[i].Min)
	}
	for i := range ind.barCode {
		ind.barCode[i] = rand.ExpFloat64()
	}
}
func (ind *Individ) Die() {
	ind.ancestors = nil
	for _, listener := range ind.deathListeners {
		listener.IsDead(ind)
	}
}
func (ind *Individ) Survive() {
	for _, listener := range ind.survivalListeners {
		listener.Survived(ind)
	}
	for _, callback := range ind.survivalCallbacks {
		callback(ind)
	}
	ind.survivalListeners = nil
}
func (parent *Individ) Survived(child *Individ) {
	parent.childSurvivorCount++
}
func (one *Individ) IdenticalTo(other *Individ) (identical bool) {
	for i := range one.genome {
		if one.genome[i] != other.genome[i] {
			return false
		}
	}
	return true
}

// Distance Methods
func (ind *Individ) DistanceTo(other *Individ) float64 {
	return vecmat.Distance(ind.genome, other.genome)
}
func (ind *Individ) DistanceManhattanTo(other *Individ) float64 {
	return vecmat.DistanceManhattan(ind.genome, other.genome)
}
func (ind *Individ) DistanceFractionalTo(other *Individ) float64 {
	return vecmat.DistanceUnit(ind.genome, other.genome, ind.blueprint.Ranges())
}
func (ind *Individ) DistanceManhattanFractionalTo(other *Individ) float64 {
	return vecmat.DistanceUnitManhattan(ind.genome, other.genome, ind.blueprint.Ranges())
}
func (ind *Individ) DistanceUnitTo(other *Individ, unitVector []float64) float64 {
	return vecmat.DistanceUnit(ind.genome, other.genome, unitVector)
}
func (ind *Individ) DistanceManhattanUnitTo(other *Individ, unitVector []float64) float64 {
	return vecmat.DistanceUnitManhattan(ind.genome, other.genome, unitVector)
}
func (ind *Individ) closestNeighboor(group []*Individ) int {
	var (
		closest float64
		index   int
	)

	if group[0] != ind {
		index = 0
	} else {
		index = 1
	}
	closest = ind.DistanceTo(group[index])

	for i := index + 1; i < len(group); i++ {
		if distance := ind.DistanceTo(group[i]); distance < closest && group[i] != ind {
			closest = distance
			index = i
		}
	}
	return index
}

// Listeners
func (ind *Individ) AddDeathListener(listener DeathListener) {
	ind.deathListeners = append(ind.deathListeners, listener)
}
func (ind *Individ) RemoveDeathListener(listener DeathListener) {
	for i := 0; i < len(ind.deathListeners); i++ {
		if ind.deathListeners[i] == listener {
			ind.deathListeners[i] = ind.deathListeners[len(ind.deathListeners)-1]
			ind.deathListeners = ind.deathListeners[:len(ind.deathListeners)-1]
			break
		}
	}
}
func (ind *Individ) AddSurvivalListener(listener SurvivalListener) {
	ind.survivalListeners = append(ind.survivalListeners, listener)
}
func (ind *Individ) AddSurvivalCallback(callback func(survivor *Individ)) {
	ind.survivalCallbacks = append(ind.survivalCallbacks, callback)
}
func (ind *Individ) RemoveSurvivalListener(listener SurvivalListener) {
	for i := 0; i < len(ind.survivalListeners); i++ {
		if ind.survivalListeners[i] == listener {
			ind.survivalListeners[i] = ind.survivalListeners[len(ind.survivalListeners)-1]
			ind.survivalListeners = ind.survivalListeners[:len(ind.survivalListeners)-1]
			break
		}
	}
}

// Creator
func NewIndivid(blueprint Blueprint) *Individ {
	ind := new(Individ)
	ind.blueprint = blueprint
	ind.genome = make([]float64, len(blueprint))
	ind.barCode = make([]float64, 1000)
	ind.meta = make(map[string]interface{})
	ind.testScores = make(map[string]float64)

	return ind
}

type metaCleaner string

func (name metaCleaner) IsDead(ind *Individ) {
	delete(ind.meta, string(name))
}
