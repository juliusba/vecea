package vecea

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"sort"
	"vecmat"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Distance ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerDistance struct {
	Min       bool
	Manhattan bool
	Unit      bool

	Weight       float64
	SurvivalRate float64
	UnitVector   []float64

	distValFunc func(i1, i2 *Individ, distVal *float64)

	victorCount float64
}

func (ex *ExecutionerDistance) Execute(ea *EA) {
	ea.Population.Sort()
	victores := make(Population, 1, int(ex.victorCount))
	victor := ea.Population[0]
	victores[0] = victor

	for _, individ := range ea.Population {
		individ.SetMeta("distFitness", 1/individ.fitness, false)
	}

	contestors := make([]int, len(ea.Population)-1)
	for i := 1; i < len(ea.Population)-1; i++ {
		contestors[i] = i
	}
	contestors[0] = len(ea.Population) - 1
	dists := make([]float64, len(ea.Population))
	if ex.Min {
		for i := range ea.Population {
			dists[i] = math.MaxFloat64
		}
	}

	var minVals, maxVals []float64
	if ex.Unit {
		minVals = vecmat.XVec(ea.GenomeLen(), math.MaxFloat64)
		maxVals = vecmat.XVec(ea.GenomeLen(), -math.MaxFloat64)
		ex.UnitVector = ea.Blueprint().Ranges()
	}

	for len(victores) < int(ex.victorCount) {
		var (
			maxDist        = -math.MaxFloat64
			minFit, maxFit = math.MaxFloat64, -math.MaxFloat64
		)
		for _, index := range contestors {
			ex.distValFunc(ea.Population[index], victor, &dists[index])
			minFit = math.Min(minFit, ea.Population[index].Meta("distFitness").(float64))
			maxFit = math.Max(maxFit, ea.Population[index].Meta("distFitness").(float64))
			maxDist = math.Max(maxDist, dists[index])
		}

		cIndex := 0
		bestIndex := -1
		bestMultiVal := -math.MaxFloat64
		for i, index := range contestors {
			if dists[index] == 0 {
				continue
			}
			multiVal := (ea.Population[index].Meta("distFitness").(float64)-minFit)/(maxFit-minFit) +
				ex.Weight*dists[index]/maxDist
			// multiVal := (ea.Population[index].Meta("distFitness").(float64)-meanFitness)/sdFitness +
			// 	ex.Weight*(dists[index]-meanDist)/sdDist
			if multiVal > bestMultiVal && ea.Population[index].ancestors != nil {
				bestMultiVal = multiVal
				bestIndex = index
				cIndex = i
			}
		}

		// Bug!!! Fix
		if bestIndex == -1 {
			cIndex = rand.Intn(len(contestors))
			bestIndex = contestors[cIndex]
		}
		// End Bug

		victor = ea.Population[bestIndex]
		victores = append(victores, victor)
		contestors[cIndex], contestors = contestors[len(contestors)-1], contestors[:len(contestors)-1]

		if ex.Unit {
			for i, val := range victor.genome {
				minVals[i] = math.Min(minVals[i], val)
				maxVals[i] = math.Min(maxVals[i], val)
				ex.UnitVector[i] = math.Max(maxVals[i]-minVals[i], victor.blueprint[i].Range*math.Pow10(-5))
			}
		}
	}

	for _, individ := range ea.Population {
		if Population(victores).IndexOfIndivid(individ) == -1 {
			individ.Die()
		}
	}

	ea.Population = victores
}

func (ex *ExecutionerDistance) Init(ea *EA) {
	if ex.victorCount == 0 {
		if ex.SurvivalRate != 0 {
			ex.victorCount = ex.SurvivalRate * float64(ea.generationSize)
		} else {
			ex.victorCount = float64(ea.generationSize)
		}
	}

	if !ex.Unit {
		ex.UnitVector = ea.Blueprint().Ranges()
	}
}

func (ex *ExecutionerDistance) String() string {
	return fmt.Sprintf("(Executioner)\t Distance (Min: %t, Manhattan: %t, Normalized: %t, Weight: %v, SurvivalRate: %v)",
		ex.Min, ex.Manhattan, ex.Unit, ex.Weight, ex.SurvivalRate)
}

func NewExecutionerDistance(weight float64, survivalRate float64, min, manhattan, unit bool) (ex *ExecutionerDistance) {
	ex = new(ExecutionerDistance)
	ex.Weight = weight
	ex.SurvivalRate = survivalRate
	ex.Min = min
	ex.Manhattan = manhattan
	ex.Unit = unit

	var distFunc func(i1, i2 *Individ) float64
	if manhattan {
		distFunc = func(i1, i2 *Individ) float64 {
			return i1.DistanceManhattanUnitTo(i2, ex.UnitVector)
		}
	} else {
		distFunc = func(i1, i2 *Individ) float64 {
			return i1.DistanceUnitTo(i2, ex.UnitVector)
		}
	}

	if min {
		ex.distValFunc = func(subject, victor *Individ, distVal *float64) {
			dist := distFunc(subject, victor)
			*distVal = math.Min(*distVal, dist)
		}
	} else {
		ex.distValFunc = func(subject, victor *Individ, distVal *float64) {
			dist := distFunc(subject, victor)
			if dist == 0 {
				*distVal = 0
			} else {
				*distVal += dist
			}
		}
	}

	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ RankDist ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerRankDist struct {
	*ExecutionerDistance
}

type _distRanker struct {
	dists   []float64
	indexes []int
}

func _NewDistRanker(length int) (ranker *_distRanker) {
	ranker = new(_distRanker)
	ranker.dists = make([]float64, length)
	ranker.indexes = make([]int, length)
	return
}

func (ranker *_distRanker) Len() int {
	return len(ranker.indexes)
}

func (ranker *_distRanker) Less(i, j int) bool {
	i, j = ranker.indexes[i], ranker.indexes[j]
	return ranker.dists[i] > ranker.dists[j]
}

func (ranker *_distRanker) Swap(i, j int) {
	temp := ranker.indexes[i]
	ranker.indexes[i] = ranker.indexes[j]
	ranker.indexes[j] = temp
}

func (ex *ExecutionerRankDist) Execute(ea *EA) {
	victores := make(Population, 1, int(ex.victorCount))
	ea.Population.Sort()
	victor := ea.Population[0]
	victores[0] = victor

	contestors := make([]int, len(ea.Population)-1)
	for i := 1; i < len(ea.Population); i++ {
		contestors[i-1] = i
	}

	dists := _NewDistRanker(len(ea.Population) - 1)
	if ex.Min {
		for i := range dists.dists {
			dists.dists[i] = math.MaxFloat64
		}
	}
	if ex.Unit {
		ex.UnitVector = ea.Blueprint().Ranges()
	}

	for len(victores) < int(ex.victorCount) {
		for i := range dists.indexes {
			dists.indexes[i] = i
		}
		for i, index := range contestors {
			ex.distValFunc(ea.Population[index], victor, &dists.dists[i])
		}

		sort.Sort(dists)

		cIndex := 0
		bestIndex := -1
		bestMultiVal := -math.MaxFloat64
		for i, index := range contestors {
			multiVal := float64(len(contestors)-i) + ex.Weight*float64(len(dists.indexes)-dists.indexes[i])
			if multiVal > bestMultiVal && ea.Population[index].ancestors != nil {
				bestMultiVal = multiVal
				bestIndex = index
				cIndex = i
			}
		}

		if bestIndex == -1 {
			cIndex = rand.Intn(len(contestors))
			bestIndex = contestors[cIndex]
		}
		victor = ea.Population[bestIndex]
		victores = append(victores, victor)
		contestors = append(contestors[:cIndex], contestors[cIndex+1:]...)
		dists.dists = append(dists.dists[:dists.indexes[cIndex]], dists.dists[dists.indexes[cIndex]+1:]...)
		dists.indexes = append(dists.indexes[:cIndex], dists.indexes[cIndex+1:]...)

		if ex.Unit {
			vectorStats := victores.StatsGenomeDistribution()
			for i, stats := range vectorStats {
				ex.UnitVector[i] = math.Max(stats.SD, math.Pow10(-2)*victor.blueprint[i].Range)
			}
		}
	}

	for _, individ := range ea.Population {
		if Population(victores).IndexOfIndivid(individ) == -1 {
			individ.Die()
		}
	}

	ea.Population = victores
}

func (ex *ExecutionerRankDist) String() string {
	return fmt.Sprintf("(Executioner)\t RankDist (Min: %t, Manhattan: %t, Normalized: %t, Weight: %v, SurvivalRate: %v)",
		ex.Min, ex.Manhattan, ex.Unit, ex.Weight, ex.SurvivalRate)
}

func NewExecutionerRankDist(weight float64, survivalRate float64, min, manhattan, unit bool) (ex *ExecutionerRankDist) {
	ex = new(ExecutionerRankDist)
	ex.ExecutionerDistance = NewExecutionerDistance(weight, survivalRate, min, manhattan, unit)

	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- RankContribution ------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerContributionRank struct {
	Weight       float64
	SurvivalRate float64
	genomeStats  []Stats

	distValFunc func(i1, i2 *Individ, distVal *float64)

	victorCount float64
}

func (ex *ExecutionerContributionRank) Execute(ea *EA) {
	victores := make(Population, 1, int(ex.victorCount))
	ea.Population.Sort()
	victor := ea.Population[0]
	victores[0] = victor

	contestors := make([]int, len(ea.Population)-1)
	for i := 1; i < len(ea.Population); i++ {
		contestors[i-1] = i
	}

	dists := _NewDistRanker(len(ea.Population) - 1)

	for len(victores) < int(ex.victorCount) {
		ex.genomeStats = victores.StatsGenomeDistribution()
		for i := range dists.indexes {
			dists.indexes[i] = i
		}
		for i, index := range contestors {
			contestor := ea.Population[index]
			for g, val := range contestor.genome {
				if diff := ex.genomeStats[g].Min - val; diff > 0 {
					dists.dists[i] += diff / ex.genomeStats[g].SD
				} else if diff := val - ex.genomeStats[g].Max; diff > 0 {
					dists.dists[i] += diff / ex.genomeStats[g].SD
				}
			}
		}

		sort.Sort(dists)

		cIndex := 0
		bestIndex := -1
		bestMultiVal := -math.MaxFloat64
		for i, index := range contestors {
			multiVal := float64(len(contestors)-i) +
				float64(len(dists.indexes)-dists.indexes[i])*ex.Weight
			if multiVal > bestMultiVal {
				bestMultiVal = multiVal
				bestIndex = index
				cIndex = i
			}
		}

		if bestIndex == -1 {
			cIndex = rand.Intn(len(contestors))
			bestIndex = contestors[cIndex]
		}
		victor = ea.Population[bestIndex]
		victores = append(victores, victor)
		contestors = append(contestors[:cIndex], contestors[cIndex+1:]...)
		dists.dists = append(dists.dists[:dists.indexes[cIndex]], dists.dists[dists.indexes[cIndex]+1:]...)
		dists.indexes = append(dists.indexes[:cIndex], dists.indexes[cIndex+1:]...)
	}

	for _, individ := range ea.Population {
		if Population(victores).IndexOfIndivid(individ) == -1 {
			individ.Die()
		}
	}

	ea.Population = victores
}

func (ex *ExecutionerContributionRank) Init(ea *EA) {
	if ex.victorCount == 0 {
		if ex.SurvivalRate != 0 {
			ex.victorCount = ex.SurvivalRate * float64(ea.generationSize)
		} else {
			ex.victorCount = float64(ea.generationSize)
		}
	}
}

func (ex *ExecutionerContributionRank) String() string {
	return fmt.Sprintf("(Executioner)\t Contribution (Weight: %v, SurvivalRate: %v)",
		ex.Weight, ex.SurvivalRate)
}

func NewExecutionerContributionRank(weight float64, survivalRate float64) (ex *ExecutionerContributionRank) {
	ex = new(ExecutionerContributionRank)
	ex.Weight = weight
	ex.SurvivalRate = survivalRate

	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- RankDistHybrid --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerRankDistHybrid struct {
	*ExecutionerDistance
}

func (ex *ExecutionerRankDistHybrid) Execute(ea *EA) {
	victores := make(Population, 1, int(ex.victorCount))
	ea.Population.Sort()
	victor := ea.Population[0]
	victores[0] = victor

	contestors := make([]int, len(ea.Population)-1)
	for i := 1; i < len(ea.Population); i++ {
		contestors[i-1] = i
	}
	dists := make([]float64, len(ea.Population)-1)
	if ex.Min {
		for i := range dists {
			dists[i] = math.MaxFloat64
		}
	}
	if ex.Unit {
		ex.UnitVector = ea.Blueprint().Ranges()
	}

	for len(victores) < int(ex.victorCount) {
		for i, index := range contestors {
			ex.distValFunc(ea.Population[index], victor, &dists[i])
		}

		cIndex := 0
		bestIndex := -1
		bestMultiVal := -math.MaxFloat64
		distStats := NewStats(dists, false)
		for i, index := range contestors {
			distVal := (dists[i] - distStats.Min) / (distStats.Max - distStats.Min)
			if distVal > 1.0 {
				log.Fatal("Should not get here")
			}
			multiVal := float64(len(contestors)-i) + ex.Weight*distVal*float64(len(contestors))
			if multiVal > bestMultiVal && ea.Population[index].ancestors != nil {
				bestMultiVal = multiVal
				bestIndex = index
				cIndex = i
			}
		}

		if bestIndex == -1 {
			cIndex = rand.Intn(len(contestors))
			bestIndex = contestors[cIndex]
		}
		victor = ea.Population[bestIndex]
		victores = append(victores, victor)
		contestors = append(contestors[:cIndex], contestors[cIndex+1:]...)
		dists = append(dists[:cIndex], dists[cIndex+1:]...)

		if ex.Unit {
			vectorStats := victores.StatsGenomeDistribution()
			for i, stats := range vectorStats {
				ex.UnitVector[i] = math.Max(stats.SD, math.Pow10(-2)*victor.blueprint[i].Range)
			}
		}
	}

	for _, individ := range ea.Population {
		if Population(victores).IndexOfIndivid(individ) == -1 {
			individ.Die()
		}
	}

	ea.Population = victores
}

func (ex *ExecutionerRankDistHybrid) String() string {
	return fmt.Sprintf("(Executioner)\t RankDistHybrid (Min: %t, Manhattan: %t, Normalized: %t, Weight: %v, SurvivalRate: %v)",
		ex.Min, ex.Manhattan, ex.Unit, ex.Weight, ex.SurvivalRate)
}

func NewExecutionerRankDistHybrid(weight float64, survivalRate float64, min, manhattan, unit bool) (ex *ExecutionerRankDistHybrid) {
	ex = new(ExecutionerRankDistHybrid)
	ex.ExecutionerDistance = NewExecutionerDistance(weight, survivalRate, min, manhattan, unit)

	return
}
