package vecea

import (
	"fmt"
	"log"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Explorer --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

// // OperatorMinDist sets the expected stepSize of the mutation to be the distance to the parents closest neighboor.
// type OperatorExplorer struct {
// 	ExplorationR  float64
// 	ExploitationR float64
// }

// func (op *OperatorExplorer) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
// 	return false
// }

// func (op *OperatorExplorer) Update(ea *EA) {
// 	if ea.stats.DistanceMean < op.ExploitationR {
// 		factor := (op.ExplorationR / ea.stats.DistanceMean)
// 		for _, individ := range ea.population {
// 			for i := 0; i < len(individ.genome); i++ {
// 				individ.genome[i] += (individ.genome[i] - ea.stats.AvgPoint[i]) * factor
// 			}
// 			if ea.populationTester != nil {
// 				ea.populationTester.TestPopulation(ea.population)
// 			} else {
// 				for _, individ := range ea.population {
// 					individ.fitness = ea.individTester.TestIndivid(individ)
// 				}
// 			}
// 		}

// 		// log.Fatal(ea.population.DistanceMean())
// 	}
// }

// func (op *OperatorExplorer) String() string {
// 	return fmt.Sprintf("(Operator)\t Explorer (ExploitationRadius: %v, ExplorationRadius: %v)", op.ExplorationR, op.ExploitationR)
// }

// func NewOperatorExplorer(explorationR, exploitationR float64) *OperatorExplorer {
// 	op := new(OperatorExplorer)
// 	op.ExplorationR = explorationR
// 	op.ExploitationR = exploitationR

// 	return op
// }

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- WinningTeam -------------------------------------------------
// -------------------------------------------------------------------------------------------------------

// OperatorMinDist sets the expected stepSize of the mutation to be the distance to the parents closest neighboor.
type OperatorWinningTeam struct {
	ParentCount int
}

func (op *OperatorWinningTeam) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < op.ParentCount; i++ {
		if parent := parents[i]; parent.age == 1 {
			if grandParent := parent.Parent(); grandParent != nil {
				for i := 0; i < len(parent.genome); i++ {
					mutVec[i] = parent.genome[i] - grandParent.genome[i]
				}
				return true
			}
		}
	}
	return false
}

func (op *OperatorWinningTeam) String() string {
	return fmt.Sprintf("(Operator)\t WinningTeam (ParentCount: %v)", op.ParentCount)
}

func (op *OperatorWinningTeam) ParentDemand() int {
	return op.ParentCount
}

func NewOperatorWinningTeam(parentCount int) *OperatorWinningTeam {
	if parentCount < 1 {
		log.Fatal("Error (vecea.OperatorWinningTeam): ParentCount must be 1 or more!")
	}

	op := new(OperatorWinningTeam)
	op.ParentCount = parentCount

	return op
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Repulsion -------------------------------------------------
// -------------------------------------------------------------------------------------------------------

// OperatorMinDist sets the expected stepSize of the mutation to be the distance to the parents closest neighboor.
// type OperatorRepulsion struct {
// 	GenCount        int
// 	RepulsionFactor float64
// 	FitnessFactor   float64
// 	DistanceFactor  float64

// 	previousTests            []*Individ
// 	fitnessMean, fitnessSD   float64
// 	distanceMean, distanceSD float64

// 	children   []*Individ
// 	ghosts     [][]*Individ
// 	ghostIndex int
// }

// func (op *OperatorRepulsion) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
// 	original := parents[0]

// 	repulsion := op.getRepulsion(original)

// 	for i := 0; i < len(mutVec); i++ {
// 		mutVec[i] *= repulsion[i]
// 	}

// 	op.children = append(op.children, child)
// 	child.AddDeathListener(op)

// 	return false
// }

// func (op *OperatorRepulsion) getRepulsion(original *Individ) []float64 {
// 	repulsion := make([]float64, len(original.genome))
// 	for _, individ := range op.previousTests {
// 		// Potential: Replace fitnessMean with original.fitness, and fitnessSD with maxFitness-MinFitness
// 		relativeFitness := (individ.fitness - original.fitness) / op.fitnessSD
// 		relativeDistance := (vecmat.ManhattanDistance(original.genome, individ.genome) - op.distanceMean) / op.distanceSD
// 		differenceVec := vecmat.Translate(
// 			original.genome,
// 			vecmat.NegateNew(individ.genome))
// 		vecmat.Multiply(
// 			differenceVec,
// 			-(op.FitnessFactor*relativeFitness + op.DistanceFactor*relativeDistance))
// 		repulsion = vecmat.Translate(repulsion, differenceVec)
// 	}
// 	vecmat.Multiply(repulsion, 1.0/float64(len(op.previousTests)))

// 	return repulsion
// }

// func (op *OperatorRepulsion) Init(ea *EA) {
// 	op.previousTests = make([]*Individ, 0, len(ea.population)*op.GenCount)
// 	op.previousTests = append(op.previousTests, ea.population...)
// 	for i := 0; i < len(ea.population); i++ {
// 		op.previousTests[i].AddDeathListener(op)
// 	}

// 	op.ghosts = make([][]*Individ, op.GenCount)
// 	for i := 0; i < op.GenCount; i++ {
// 		op.ghosts[i] = make([]*Individ, 0, ea.generationSize)
// 	}

// 	op.updateVals()
// }

// func (op *OperatorRepulsion) Update(ea *EA) {
// 	// Add newcommers
// 	op.previousTests = append(op.previousTests, op.children...)
// 	op.children = op.children[:0]

// 	// Kill ghosts
// 	op.ghostIndex = (op.ghostIndex + 1) % len(op.ghosts)
// 	indexes := make([]int, len(op.ghosts[op.ghostIndex]))
// 	for i, ghost := range op.ghosts[op.ghostIndex] {
// 		index := Population(op.previousTests).IndexOf(ghost)
// 		indexes[i] = index
// 	}
// 	sort.Ints(indexes)

// 	for i := len(indexes) - 1; i >= 0; i-- {
// 		op.previousTests[indexes[i]] = op.previousTests[len(op.previousTests)-1]
// 		op.previousTests = op.previousTests[:len(op.previousTests)-1]
// 	}
// 	op.ghosts[op.ghostIndex] = op.ghosts[op.ghostIndex][:0]

// 	// Update Values
// 	op.updateVals()
// }

// func (op *OperatorRepulsion) updateVals() {
// 	op.fitnessMean = Population(op.previousTests).Mean()
// 	op.fitnessSD = Population(op.previousTests).SD(op.fitnessMean)
// 	op.distanceMean = Population(op.previousTests).DistanceMean()
// 	op.distanceSD = Population(op.previousTests).DistanceSD(op.distanceMean)
// }

// func (op *OperatorRepulsion) IsDead(corpse *Individ) {
// 	op.ghosts[op.ghostIndex] = append(op.ghosts[op.ghostIndex], corpse)
// }

// func (op *OperatorRepulsion) String() string {
// 	return fmt.Sprintf("(Operator)\t Repulsion (GenCount: %v, RepelFactor: %v, FitnessFactor: %v, DistanceFactor: %v)",
// 		op.GenCount, op.RepulsionFactor, op.FitnessFactor, op.DistanceFactor)
// }

// func NewOperatorRepulsion(genCount int, repulsionFactor, fitnessFactor float64) *OperatorRepulsion {
// 	if genCount < 1 {
// 		log.Fatal("vecea.NewOperatorRepulsion: genCount must be 1 or more!")
// 	}

// 	op := new(OperatorRepulsion)
// 	op.GenCount = genCount
// 	op.RepulsionFactor = repulsionFactor
// 	op.FitnessFactor = fitnessFactor

// 	return op
// }
