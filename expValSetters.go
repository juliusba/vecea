package vecea

import (
	"fmt"
	"math"
)

type ExpValSetter interface {
	SetExpVals(*EA)
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------- Rank -----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExpValRank struct {
	Ratio   float64
	expVals []float64
}

func (exp *ExpValRank) SetExpVals(ea *EA) {
	if len(ea.Population) != len(exp.expVals) {
		exp.newExpVals(len(ea.Population))
	}

	ea.Population.Sort()
	for i, individ := range ea.Population {
		individ.expVal = exp.expVals[i]
	}
}

func (exp *ExpValRank) Init(ea *EA) {
	exp.newExpVals(len(ea.Population))
}

func (exp *ExpValRank) newExpVals(count int) {
	sum := ((exp.Ratio-1)/2 + 1) * float64(count)
	stepSize := (exp.Ratio - 1) / float64(count)
	exp.expVals = make([]float64, count)
	expVal := exp.Ratio
	for i := 0; i < count; i++ {
		exp.expVals[i] = expVal / sum
		expVal -= stepSize
	}
}

func (exp *ExpValRank) String() string {
	return fmt.Sprintf("(ExpValSetter)\t Rank (Ratio: %v)", exp.Ratio)
}

func NewExpValRank(ratio float64) *ExpValRank {
	expSetter := new(ExpValRank)
	expSetter.Ratio = ratio

	return expSetter
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Rank NonLinear --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExpValRankNonLinear struct {
	SP float64
}

func (exp *ExpValRankNonLinear) SetExpVals(ea *EA) {
	ea.Population.Sort()
	sum := 0.0
	for i, individ := range ea.Population {
		individ.expVal = exp.SP * math.Pow(1-exp.SP, float64(i))
		sum += individ.expVal
	}
	for _, individ := range ea.Population {
		individ.expVal /= sum
	}
}

func (exp *ExpValRankNonLinear) String() string {
	return fmt.Sprintf("(ExpValSetter)\t Rank NonLinear (SP: %v)", exp.SP)
}

func NewExpValRankNonLinear(selectionPressure float64) *ExpValRankNonLinear {
	expSetter := new(ExpValRankNonLinear)
	expSetter.SP = selectionPressure

	return expSetter
}

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- ChildCount --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExpValChildCount struct {
	Ratio float64
}

func (exp *ExpValChildCount) SetExpVals(ea *EA) {
	ea.Population.Sort()
	sum := 0.0
	for _, individ := range ea.Population {
		individ.expVal = (1 - exp.Ratio) + exp.Ratio*1/(1+math.Log2(float64(individ.childCount)))
		sum += individ.expVal
	}
	for _, individ := range ea.Population {
		individ.expVal /= sum
	}
}

func (exp *ExpValChildCount) String() string {
	return fmt.Sprintf("(ExpValSetter)\t ChildCount")
}

func NewExpValChildCount(ratio float64) *ExpValChildCount {
	expSetter := new(ExpValChildCount)
	expSetter.Ratio = ratio

	return expSetter
}
