package vecea

import (
	"errors"
	"fmt"
	"math"
)

type Blueprinter interface {
	Blueprint() Blueprint
	GenomeLen() int
	Phenotype(individ *Individ) interface{}
}

type Blueprint []BlueprintElement

func (bp Blueprint) PhenoVals(genome []float64) (phenoVals []float64) {
	phenoVals = make([]float64, len(bp))
	for i, el := range bp {
		if el.Exp {
			if el.Negative {
				phenoVals[i] = -math.Exp(genome[i])
			} else {
				phenoVals[i] = math.Exp(genome[i])
			}
		} else {
			phenoVals[i] = genome[i]
		}
	}
	return
}

func (bp Blueprint) Ranges() (ranges []float64) {
	ranges = make([]float64, len(bp))
	for i, el := range bp {
		ranges[i] = el.Range
	}
	return
}

func (bp Blueprint) DistanceManhattan(one, other *Individ) (dist float64) {
	for i, el := range bp {
		if el.Circular {
			dist += math.Abs(math.Abs(one.genome[i]-el.Center) - math.Abs(other.genome[i]-el.Center))
		} else {
			dist += math.Abs(one.genome[i] - other.genome[i])
		}
	}
	return
}

type BlueprintElement struct {
	Min    float64
	Max    float64
	Center float64
	Range  float64
	Name   string

	Exp      bool
	Negative bool
	Circular bool
}

func NewBlueprintElement(min, max float64, name string) BlueprintElement {
	bpel := BlueprintElement{}
	bpel.Min = min
	bpel.Max = max
	bpel.Center = min + (max-min)/2
	bpel.Range = max - min

	return bpel
}

func NewBlueprintElementExp(min, max float64, name string) BlueprintElement {
	bpel := BlueprintElement{}
	bpel.Exp = true

	if min*max <= 0 {
		panic(errors.New(fmt.Sprintf("Trying to create exponential gene for a range that includes 0: %v", bpel)))
	} else if max < 0 {
		bpel.Negative = true
		max = -min
		min = -max
	}

	bpel.Min = math.Log(min)
	bpel.Max = math.Log(max)
	bpel.Range = max - min

	return bpel
}

func (one *BlueprintElement) Equals(other *BlueprintElement) bool {
	return one.Name == other.Name && one.Min == other.Min &&
		one.Max == other.Max && one.Exp == other.Exp
}
