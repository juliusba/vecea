package vecea

func NewEAStandard(generationSize int, bp Blueprinter) (ea *EA) {
	ea = NewEA(generationSize, 0.1, 0, 0, nil, bp, NewExpValRankNonLinear(0.02),
		NewSelectorRoulette(), NewExecutionerStandard(0),
		NewOperatorRate(0.1),
		NewOperatorGauss(),
		NewOperatorScaleRange(0.2, true),
		NewOperatorSinglePoint())
	return
}

func NewEADelta(generationSize int, bp Blueprinter) (ea *EA) {
	ea = NewEA(generationSize, 1.0, 0, 0, nil, bp, NewExpValRankNonLinear(0.02),
		NewSelectorRoulette(), NewExecutionerStandard(0), //;NewExecutionerRankDistHybrid(1.0, 0.5, true, true, true),
		NewOperatorRate(0.1), NewOperatorDelta(1.5, 2, 1.0, false, false), NewOperatorNPoint(2, 0.25))
	return
}

func NewEADeltaNeighbour(generationSize int, bp Blueprinter) (ea *EA) {
	ea = NewEA(generationSize, 1.0, 0, 0, nil, bp, NewExpValRankNonLinear(0.02),
		NewSelectorRoulette(), NewExecutionerNeighbourKiller(1.0),
		NewOperatorRate(0.25), NewOperatorDelta(1.5, 2, 1.0, false, false), NewOperatorNPoint(2, 0.25))
	return
}

func NewEADiff(generationSize int, bp Blueprinter) (ea *EA) {
	ea = NewEA(generationSize, 1.0, 0, 0, nil, bp, nil,
		NewSelectorIncremental(), NewExecutionerStandard(0),
		NewOperatorDifferential(0.9, 0.5))
	return
}

func NewEAAdaDiff(generationSize int, bp Blueprinter) (ea *EA) {
	ea = NewEA(generationSize, 1.0, 0, 0, nil, bp, nil,
		NewSelectorIncremental(), NewExecutionerOedipus(),
		NewOperatorSADE(0.1, 0.1, 1.0, 0.1, 0.0, 1.0))
	return
}
