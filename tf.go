package vecea

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
	"vecmat"
)

type TestFunction struct {
	tester   TF
	testName string

	dimensions int
	blueprint  Blueprint

	rotationMatrix    *vecmat.Matrix
	translationVector []float64
}

func (tefu *TestFunction) TestSolution(solution interface{}) (score float64) {
	score = tefu.tester.Test(solution.([]float64))
	return
}

func (tefu *TestFunction) TestName() (name string) {
	return "tf"
}

func (tefu *TestFunction) IsFinished(individ *Individ) (isFinished bool) {
	if tefu.tester.globalOptimas != nil {
		solution := individ.Phenotype.([]float64)
		dist := 0.0
		for i := 0; i < len(solution); i += len(tefu.tester.BPloop) {
			minDist := math.MaxFloat64
			for _, optimum := range tefu.tester.globalOptimas {
				temp := 0.0
				for loopIndex := range optimum {
					temp += math.Abs(solution[i+loopIndex]-optimum[loopIndex]) / tefu.tester.BPloop[loopIndex].Range
				}
				minDist = math.Min(minDist, temp)
			}
			dist += minDist
		}
		isFinished = dist/float64(len(solution)) < math.Pow10(-5)
	}
	return
}

func (tefu *TestFunction) Blueprint() Blueprint {
	return tefu.blueprint
}

func (tefu *TestFunction) GenomeLen() int {
	return tefu.dimensions
}

func (tefu *TestFunction) Phenotype(individ *Individ) interface{} {
	solution := individ.Genome()

	if tefu.rotationMatrix != nil {
		solution = vecmat.Rotate(solution, tefu.rotationMatrix)
	}
	if tefu.translationVector != nil {
		solution = vecmat.Translate(solution, tefu.translationVector)
	}
	return solution
}

func (tefu *TestFunction) String() string {
	return fmt.Sprintf(
		"(TestFunction)\t %s (D: %v, BP loop: %v, Translation: %t, Rotation: %t",
		tefu.testName,
		tefu.dimensions, tefu.tester.BPloop, tefu.translationVector != nil, tefu.rotationMatrix != nil)
}

func NewTestFunction(tester TF, dimensions int, translation, rotation bool) *TestFunction {
	tefu := new(TestFunction)

	tefu.tester = tester
	tefu.dimensions = dimensions
	tefu.blueprint = tester.BP(dimensions)

	if translation {
		tefu.translationVector = vecmat.RandVec(dimensions)
		vecmat.Multiply(tefu.translationVector, tester.MinRange()/4.0)
	}
	if rotation {
		tefu.rotationMatrix = vecmat.RandomRotationMatrix(dimensions)
	}

	tefu.testName = runtime.FuncForPC(reflect.ValueOf(tefu.tester._test).Pointer()).Name()

	return tefu
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Testfunctions ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type TF struct {
	BPloop        Blueprint
	goal          float64
	globalOptimas [][]float64

	_test func(solution []float64) (score float64)
}

func (tf *TF) Test(solution []float64) (score float64) {
	score = tf._test(solution)
	return
}

func (tf *TF) BP(dimensions int) (bp Blueprint) {
	bp = make(Blueprint, 0, dimensions)
	for i := 0; i < cap(bp)-len(tf.BPloop)+1; i += len(tf.BPloop) {
		bp = append(bp, tf.BPloop...)
	}
	for i := 0; i < cap(bp)%len(tf.BPloop); i++ {
		bp = append(bp, tf.BPloop[i])
	}
	return
}

func (tf *TF) MinRange() (minRange float64) {
	minRange = math.MaxFloat64
	for _, el := range tf.BPloop {
		minRange = math.Min(minRange, el.Range)
	}
	return
}

func NewTFAckley() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-32, 32, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{0}}
	tf._test = TFAckley
	return
}

func TFAckley(solution []float64) (score float64) {
	sum1 := 0.0
	sum2 := 0.0
	for i := 0; i < len(solution); i++ {
		x := solution[i]
		sum1 += x * x
		sum2 += math.Cos(2 * math.Pi * x)
	}
	divN := 1.0 / float64(len(solution))
	score = (20 + math.E -
		20*math.Exp(-0.2*math.Sqrt(divN*sum1)) -
		math.Exp(divN*sum2))

	return
}

func NewTFAckley2() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-5, 5, "X"),
		NewBlueprintElement(-5, 5, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{0, 0}}
	tf._test = TFAckley2
	return
}

func TFAckley2(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += -20*math.Exp(-0.2*math.Sqrt(0.5*(x*x+y*y))) -
			math.Exp(0.5*(math.Cos(2*math.Pi*x)+math.Cos(2*math.Pi*y))) + math.E + 20

	}
	return
}

func NewTFSphere() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-100, 100, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{0}}
	tf._test = TFSphere
	return
}

func TFSphere(solution []float64) (score float64) {
	for _, x := range solution {
		score += x * x
	}
	return
}

func NewTFGriewank() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-600, 600, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{0}}
	tf._test = TFGriewank
	return
}

func TFGriewank(solution []float64) (score float64) {
	sum1 := 0.0
	sum2 := 1.0
	for i := 0; i < len(solution); i++ {
		x := solution[i]
		sum1 += x * x
		sum2 *= math.Cos(x / math.Sqrt(float64(i)+1.0))
	}
	score = 1 + (1.0/4000.0)*sum1 - sum2
	return
}

func NewTFRosenbrock() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-100, 100, "X"),
		NewBlueprintElement(-100, 100, "X2"),
	}
	tf.globalOptimas = [][]float64{[]float64{1, 1}}
	tf._test = TFRosenbrock
	return
}

func TFRosenbrock(solution []float64) (score float64) {
	for i := 1; i < len(solution); i++ {
		x1 := solution[i-1]
		x2 := solution[i]
		temp1 := x2 - x1*x1
		temp2 := x1 - 1
		score += 100*temp1*temp1 + temp2*temp2
	}
	return
}

func NewTFRastrigin() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-5.12, 5.12, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{0}}
	tf._test = TFRastrigin
	return
}

func TFRastrigin(solution []float64) (score float64) {
	score = 10.0 * float64(len(solution))
	for i := 0; i < len(solution); i++ {
		x := solution[i]
		score += x*x - 10*math.Cos(2*math.Pi*x)
	}
	return
}

func NewTFBeale() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-4.5, 4.5, "X"),
		NewBlueprintElement(-4.5, 4.5, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{3, 0.5}}
	tf._test = TFBeale
	return
}

func TFBeale(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += math.Pow(1.5-x+x*y, 2) + math.Pow(2.25-x+x*y*y, 2) +
			math.Pow(2.625-x+x*y*y*y, 2)
	}
	return
}

func NewTFGoldsteinPrice() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-2, 2, "X"),
		NewBlueprintElement(-2, 2, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{0, -1}}
	tf._test = TFGoldsteinPrice
	return
}

func TFGoldsteinPrice(solution []float64) (score float64) {
	score = -3 * float64(len(solution)) / 2
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += (1 + math.Pow(x+y+1, 2)*(19-14*x+3*x*x-14*y+6*x*y+3*y*y)) *
			(30 + math.Pow(2*x-3*y, 2)*(18-32*x+12*x*x+48*y-36*x*y+27*y*y))
	}

	return
}

func NewTFBooths() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-10, 10, "X"),
		NewBlueprintElement(-10, 10, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{1, 3}}
	tf._test = TFBooths
	return
}

func TFBooths(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += math.Pow(x+2*y-7, 2) + math.Pow(2*x+y-5, 2)
	}
	return
}

func NewTFBunkin() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-15, -5, "X"),
		NewBlueprintElement(-3, 3, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{-10, 1}}
	tf._test = TFBunkin
	return
}

func TFBunkin(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += 100*math.Sqrt(math.Abs(y-0.01*x*x)) + 0.01*math.Abs(x+10)
	}
	return
}

func NewTFMatyas() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-10, 10, "XY"),
		NewBlueprintElement(-10, 10, "XY"),
	}
	tf.globalOptimas = [][]float64{[]float64{0, 0}}
	tf._test = TFMatyas
	return
}

func TFMatyas(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += 0.26*(x*x+y*y) + 0.48*x*y
	}
	return
}

func NewTFLevi() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-10, 10, "X"),
		NewBlueprintElement(-10, 10, "X2"),
	}
	tf.globalOptimas = [][]float64{[]float64{1, 1}}
	tf._test = TFLevi
	return
}

func TFLevi(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		sinThreePiX := math.Sin(3 * math.Pi * x)
		sinThreePiY := math.Sin(3 * math.Pi * x)
		sinTwoPiY := math.Sin(2 * math.Pi * y)
		score += sinThreePiX*sinThreePiX + math.Pow(x-1, 2)*(1+sinThreePiY*sinThreePiY) +
			math.Pow(y-1, 2)*(1+sinTwoPiY*sinTwoPiY)
	}
	return
}

func NewTFLevy2() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-10, 10, "Xi"),
	}
	tf.globalOptimas = [][]float64{[]float64{1}}
	tf._test = TFLevy2
	return
}

func TFLevy2(solution []float64) (score float64) {
	last := 1 + (solution[len(solution)-1]-1)/4
	score = math.Pow(math.Sin(math.Pi*(1+(solution[0]-1)/4)), 2) + math.Pow(last-1, 2)*(1+math.Pow(math.Sin(2*math.Pi*last), 2))
	for i := 0; i < len(solution)-1; i++ {
		w := (1 + (solution[i]-1)/4)
		score += math.Pow(w-1, 2) * (1 + 10*math.Pow(math.Sin(math.Pi*w+1), 2))
	}
	return
}

func NewTFThreeHump() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-5, 5, "XY"),
		NewBlueprintElement(-5, 5, "XY"),
	}
	tf.globalOptimas = [][]float64{[]float64{0, 0}}
	tf._test = TFThreeHump
	return
}

func TFThreeHump(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += 2*x*x - 1.05*math.Pow(x, 4) + math.Pow(x, 6)/6 + x*y + y*y
	}
	return
}

func NewTFEasom() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-100, 100, "XY"),
		NewBlueprintElement(-100, 100, "XY"),
	}
	tf.globalOptimas = [][]float64{[]float64{math.Pi, math.Pi}}
	tf._test = TFEasom
	return
}

func TFEasom(solution []float64) (score float64) {
	score = float64(len(solution)) / 2
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += -math.Cos(x) * math.Cos(y) * math.Exp(-(math.Pow(x-math.Pi, 2) +
			math.Pow(y-math.Pi, 2)))
	}
	return
}

func NewTFCrossInTray() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-10, 10, "XY"),
		NewBlueprintElement(-10, 10, "XY"),
	}
	tf.globalOptimas = [][]float64{
		[]float64{1.34941, 1.34941},
		[]float64{1.34941, -1.34941},
		[]float64{-1.34941, 1.34941},
		[]float64{-1.34941, -1.34941},
	}
	tf._test = TFCrossInTray
	return
}

func TFCrossInTray(solution []float64) (score float64) {
	score = float64(len(solution)/2) * 2.06261 //187085

	// x = solution[0]
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += -0.0001 * math.Pow(math.Abs(math.Sin(x)*math.Sin(y)*
			math.Exp(math.Abs(100-(math.Sqrt(x*x+y*y))/math.Pi)))+1, 0.1)
	}
	return
}

func NewTFEggHolder() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-512, 512, "X"),
		NewBlueprintElement(-512, 512, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{512, 404.2319}}
	tf._test = TFEggHolder
	return
}

func TFEggHolder(solution []float64) (score float64) {
	score = 959.6407 * float64(len(solution)) / 2
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += -(y+47)*math.Sin(math.Sqrt(math.Abs(y+x/2+47))) - x*math.Sin(math.Sqrt(math.Abs(x-(y+47))))
	}
	return
}

func NewTFHolderTable() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-10, 10, "X"),
		NewBlueprintElement(-10, 10, "Y"),
	}
	tf.globalOptimas = [][]float64{
		[]float64{8.05502, 9.66459},
		[]float64{-8.05502, 9.66459},
		[]float64{8.05502, -9.66459},
		[]float64{-8.05502, -9.66459},
	}
	tf._test = TFHolderTable
	return
}

func TFHolderTable(solution []float64) (score float64) {
	score = 19.2086 * float64(len(solution)) / 2
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += -math.Abs(math.Sin(x) * math.Cos(y) * math.Exp(math.Abs(1-math.Sqrt(x*x+y*y)/math.Pi)))
	}
	return
}

func NewTFMcCormick() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-1.5, 4, "X"),
		NewBlueprintElement(-3, 4, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{-0.54719, -1.54719}}
	tf._test = TFMcCormick
	return
}

func TFMcCormick(solution []float64) (score float64) {
	score = float64(len(solution)) / 2 * 1.9133
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)
		score += math.Sin(x+y) + math.Pow(x-y, 2) - 1.5*x + 2.5*y + 1
	}
	return
}

func NewTFSchaffer2() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-100, 100, "X"),
		NewBlueprintElement(-100, 100, "Y"),
	}
	tf.globalOptimas = [][]float64{[]float64{0, 0}}
	tf._test = TFSchaffer2
	return
}

func TFSchaffer2(solution []float64) (score float64) {
	for i := 0; i < len(solution); i += 2 {
		var (
			x = solution[i]
			y = solution[i+1]
		)

		xSquared := x * x
		ySquared := y * y
		// N 4: score += 0.5 + (math.Pow(math.Cos(math.Sin(math.Abs(xSquared-ySquared))), 2)-0.5)/
		//	math.Pow(1+0.001*(xSquared+ySquared), 2)
		score += 0.5 + (math.Pow(math.Sin(xSquared-ySquared), 2)-0.5)/
			math.Pow(1+0.001*(xSquared+ySquared), 2)
	}
	return
}

func NewTFStyblinskiTang() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-5, 5, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{-2.903534}}
	tf._test = TFStyblinskiTang
	return
}

func TFStyblinskiTang(solution []float64) (score float64) {
	score = 39.166165 * float64(len(solution))
	sum := 0.0
	for _, x := range solution {
		sum += math.Pow(x, 4) - 16*math.Pow(x, 2) + 5*x
	}
	score += sum / 2
	return
}

func NewTFRotatedHyperEllipsoid() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-65.536, 65.536, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{0}}
	tf._test = TFRotatedHyperEllipsoid
	return
}

func TFRotatedHyperEllipsoid(solution []float64) (score float64) {
	sum := 0.0
	for _, x := range solution {
		sum += x * x
		score += sum
	}
	return
}

func NewTFOneMax() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-100, 100, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{1}}
	tf._test = TFOneMax
	return
}

func TFOneMax(solution []float64) (score float64) {
	for i := 0; i < len(solution); i++ {
		score += math.Abs(solution[i] - 1)
	}
	return
}

func NewTFStep() (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-100, 100, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{0}}
	tf._test = TFStep
	return
}

func TFStep(solution []float64) (score float64) {
	for _, x := range solution {
		score += math.Abs(float64(int(x)))
	}
	return
}

func NewTFForks(forkLen int) (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-1, 1, "X"),
	}
	tf.globalOptimas = [][]float64{[]float64{1}, []float64{-1}}
	tf._test = func(solution []float64) (score float64) {
		threshold := 0.0
		score = float64(len(solution))
		var neg bool = false
		for i, val := range solution {
			if i%forkLen == 0 {
				if neg {
					return
				}
				score -= math.Abs(val)
				if math.Abs(val) < threshold {
					return
				}
				neg = val < 0
			} else {
				if neg {
					score += val
					if val > -threshold {
						return
					}
				} else {
					score -= val
					if val < threshold {
						return
					}
				}
			}
		}
		return
	}
	return
}

func NewTFForksStep(forkLen int) (tf TF) {
	tf.BPloop = Blueprint{
		NewBlueprintElement(-1, 1, "X"),
	}
	tf._test = func(solution []float64) (score float64) {
		threshold := 0.0
		score = float64(len(solution))
		var neg bool = false
		for i, val := range solution {
			if i%forkLen == 0 {
				if neg {
					return
				}
				score--
				if math.Abs(val) < threshold {
					return
				}
				neg = val < 0
			} else {
				if neg {
					score--
					if val > -threshold {
						return
					}
				} else {
					score--
					if val < threshold {
						return
					}
				}
			}
		}
		return
	}

	return
}

func TFForksStep(solution []float64) (score float64) {
	threshold := 0.0
	score = float64(len(solution))
	var neg bool = false
	for i, val := range solution {
		if i%3 == 0 {
			if neg {
				return
			}
			score--
			if math.Abs(val) < threshold {
				return
			}
			neg = val < 0
		} else {
			if neg {
				score--
				if val > -threshold {
					return
				}
			} else {
				score--
				if val < threshold {
					return
				}
			}
		}
	}
	return
}
