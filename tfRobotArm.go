package vecea

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

type RobotArm struct {
	angles  []float64
	lengths []float64

	xPositions []float64
	yPositions []float64
}

func NewRobotArm(segments int) (arm *RobotArm) {
	arm = new(RobotArm)
	arm.angles = make([]float64, segments)
	arm.lengths = make([]float64, segments)
	arm.xPositions = make([]float64, segments)
	arm.yPositions = make([]float64, segments)
	return
}

type RobotArmFunc struct {
	segments   int
	endX, endY float64
}

func NewRobotArmFunc(segments int) (raf *RobotArmFunc) {
	raf = new(RobotArmFunc)
	raf.segments = segments
	rand.Seed(time.Now().UTC().UnixNano())
	lenght := 1 + rand.Float64()*float64(segments-1)
	angle := rand.Float64() * math.Pi
	raf.endX = lenght * math.Cos(angle)
	raf.endY = lenght * math.Sin(angle)
	return
}

func (raf *RobotArmFunc) TestSolution(phenotype interface{}) (score float64) {
	arm := phenotype.(*RobotArm)

	endX := arm.xPositions[raf.segments-1]
	endY := arm.yPositions[raf.segments-1]
	distFromGoal := math.Sqrt(math.Pow(endX-raf.endX, 2) + math.Pow(endY-raf.endY, 2))

	score = distFromGoal
	return
}

func (raf *RobotArmFunc) TestName() (name string) {
	name = "RobotArm"
	return
}

func (raf *RobotArmFunc) IsFinished(individ *Individ) (isFinished bool) {
	return individ.testScores[raf.TestName()] < math.Pow10(-10)
}

func (raf *RobotArmFunc) Blueprint() (bp Blueprint) {
	bp = make(Blueprint, raf.segments*2)
	for i := 0; i < raf.segments; i++ {
		bp[i*2] = NewBlueprintElement(0, 1, fmt.Sprintf("len%v", i))
		bp[i*2+1] = NewBlueprintElement(0, 2*math.Pi, fmt.Sprintf("ang%v", i))
		// bp[i*2+1].Circular = true
	}
	return
}

func (raf *RobotArmFunc) GenomeLen() (length int) {
	length = raf.segments * 2
	return
}

func (raf *RobotArmFunc) Phenotype(individ *Individ) (phenotype interface{}) {
	genome := individ.genome
	gIndex := 0
	robotArm := NewRobotArm(raf.segments)

	angle, xPosition, yPosition := 0.0, 0.0, 0.0
	for i := 0; i < raf.segments; i++ {
		robotArm.lengths[i] = genome[gIndex]
		gIndex++
		robotArm.angles[i] = genome[gIndex]
		gIndex++
		angle += robotArm.angles[i]
		xPosition += robotArm.lengths[i] * math.Cos(angle)
		yPosition += robotArm.lengths[i] * math.Sin(angle)
		robotArm.xPositions[i] = xPosition
		robotArm.yPositions[i] = yPosition
	}
	phenotype = robotArm
	return
}
