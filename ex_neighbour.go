package vecea

import (
	"fmt"
	"math"
	"math/rand"
	"vecmat"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- NeighboorKiller -------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerNeighbourKiller struct {
	adultCount   int
	survivalRate float64
	unitVector   []float64

	childSuccesses float64
	parentKills    float64
}

func (ex *ExecutionerNeighbourKiller) Execute(ea *EA) {
	ea.Population.Sort()
	victimsPerKiller := int((1.0+ex.survivalRate)/ex.survivalRate) - 1
	minVals := vecmat.XVec(ea.GenomeLen(), math.MaxFloat64)
	maxVals := vecmat.XVec(ea.GenomeLen(), -math.MaxFloat64)
	unitVector := ea.Population[0].blueprint.Ranges()
	for k := 0; k < ex.adultCount; k++ {
		killer := ea.Population[k]
		for v := 0; v < victimsPerKiller; v++ {
			neighbourIndex := k + 1
			neighbour := ea.Population[neighbourIndex]
			minDist := killer.DistanceManhattanUnitTo(neighbour, unitVector) //ea.BP.DistanceManhattan(neighbour, ex.unitVector)
			for c := k + 2; c < len(ea.Population); c++ {
				candidate := ea.Population[c]
				if dist := killer.DistanceManhattanUnitTo(candidate, unitVector); dist < minDist {
					neighbour = candidate
					minDist = dist
					neighbourIndex = c
				}
			}
			if killer.age == 0 {
				ex.childSuccesses++
				if killer.Parent() == neighbour {
					ex.parentKills++
				}
			}
			neighbour.Die()
			ea.Population = append(ea.Population[:neighbourIndex], ea.Population[neighbourIndex+1:]...)
		}

		for i, val := range killer.genome {
			minVals[i] = math.Min(minVals[i], val)
			maxVals[i] = math.Min(maxVals[i], val)
			unitVector[i] = math.Max(maxVals[i]-minVals[i], killer.blueprint[i].Range*math.Pow10(-5))
		}
	}
	ea.Population = ea.Population[:ex.adultCount]
}

func (ex *ExecutionerNeighbourKiller) Init(ea *EA) {
	ex.adultCount = int(float64(ea.generationSize)*ex.survivalRate + 0.5)
	ex.unitVector = ea.Blueprint().Ranges()
}

func (ex *ExecutionerNeighbourKiller) String() string {
	return fmt.Sprintf("(Executioner)\t NeighbourKiller (SurvivalRate: %v)", ex.survivalRate)
}

func (ex *ExecutionerNeighbourKiller) ParentKills() float64 {
	return ex.parentKills / ex.childSuccesses
}

func NewExecutionerNeighbourKiller(survivalRate float64) (ex *ExecutionerNeighbourKiller) {
	ex = new(ExecutionerNeighbourKiller)
	ex.survivalRate = survivalRate
	return
}

// -------------------------------------------------------------------------------------------------------
// ----------------------------------- NeighboorKiller FAKE ----------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerNeighbourKillerFake struct {
	adultCount   int
	survivalRate float64
}

func (ex *ExecutionerNeighbourKillerFake) Execute(ea *EA) {
	ea.Population.Sort()
	victimsPerKiller := int((1.0+ex.survivalRate)/ex.survivalRate) - 1
	for k := 0; k < ex.adultCount; k++ {
		for v := 0; v < victimsPerKiller; v++ {
			neighbourIndex := k + 1
			neighbour := ea.Population[neighbourIndex+rand.Intn(len(ea.Population)-neighbourIndex)]
			neighbour.Die()
			ea.Population = append(ea.Population[:neighbourIndex], ea.Population[neighbourIndex+1:]...)
		}
	}
	ea.Population = ea.Population[:ex.adultCount]
}

func (ex *ExecutionerNeighbourKillerFake) Init(ea *EA) {
	ex.adultCount = int(float64(ea.generationSize)*ex.survivalRate + 0.5)
}

func (ex *ExecutionerNeighbourKillerFake) String() string {
	return fmt.Sprintf("(Executioner)\t NeighbourKiller FAKE (SurvivalRate: %v)", ex.survivalRate)
}

func NewExecutionerNeighbourKillerFake(survivalRate float64) (ex *ExecutionerNeighbourKillerFake) {
	ex = new(ExecutionerNeighbourKillerFake)
	ex.survivalRate = survivalRate
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- NeighbourTournament ---------------------------------------------
// -------------------------------------------------------------------------------------------------------

type ExecutionerNeighbourTournament struct {
	adultCount   int
	survivalRate float64
	unitVector   []float64
}

func (ex *ExecutionerNeighbourTournament) Execute(ea *EA) {
	ea.Population.Sort()
	victimsPerKiller := int((1.0+ex.survivalRate)/ex.survivalRate) - 1
	for k := 0; k < ex.adultCount-1; k++ {
		killer := ea.Population[k]
		minDists := vecmat.XVec(victimsPerKiller*2, math.MaxFloat64)
		neighbours := make(Population, victimsPerKiller*2)
		for c := k + 1; c < len(ea.Population); c++ {
			candidate := ea.Population[c]
			if dist := killer.DistanceManhattanUnitTo(candidate, ex.unitVector); dist < minDists[len(minDists)-1] {
				for i, minDist := range minDists {
					if dist < minDist {
						copy(minDists[i+1:], minDists[i:])
						minDists[i] = dist
						// Insert neighbour
						copy(neighbours[i+1:], neighbours[i:])
						neighbours[i] = candidate
						break
					}
				}
			}
		}

		neighbours.Sort()
		for i := victimsPerKiller; i < len(neighbours); i++ {
			neighbourIndex := ea.Population.IndexOfIndivid(neighbours[i])
			neighbours[i].Die()
			ea.Population = append(ea.Population[:neighbourIndex], ea.Population[neighbourIndex+1:]...)
		}
	}
	ea.Population = ea.Population[:ex.adultCount]

	vectorStats := ea.Population.StatsGenomeDistribution()
	for i, stats := range vectorStats {
		ex.unitVector[i] = math.Max(stats.SD, math.Pow10(-2)*ea.Blueprint()[i].Range)
	}
}

func (ex *ExecutionerNeighbourTournament) Init(ea *EA) {
	ex.adultCount = int(float64(ea.generationSize)*ex.survivalRate + 0.5)
	ex.unitVector = ea.Blueprint().Ranges()
}

func (ex *ExecutionerNeighbourTournament) String() string {
	return fmt.Sprintf("(Executioner)\t NeighbourKiller (SurvivalRate: %v)", ex.survivalRate)
}

func NewExecutionerNeighbourTournament(survivalRate float64) (ex *ExecutionerNeighbourTournament) {
	ex = new(ExecutionerNeighbourTournament)
	ex.survivalRate = survivalRate
	return
}
