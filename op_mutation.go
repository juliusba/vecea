package vecea

import (
	"fmt"
	"math"
	"math/rand"
	"vecmat"
)

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------------- Delta --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

const (
	CONST_SQRT_2_PI = 0.7978845608
	CONST_2_PI      = 0.6366197723675813430755350534900574481378385829618257
)

type OperatorDelta struct {
	ea               *EA
	Tau              float64
	Norms            int
	ConvergenceBias  float64
	CrossConvergence bool

	AdaptationRecord *deltaAdaptationRecord
	mean             float64
}

type deltaAdaptationRecord struct {
	TotalCount, Value, Mean                                 float64
	TotalAbs, MutAbs, AlienAbs, CombinedAbs                 float64
	Total, Mut, Alien, Combined                             float64
	TotalFrac, MutFrac, AlienFrac, CombinedFrac             float64
	TotalPosFrac, MutPosFrac, AlienPosFrac, CombinedPosFrac float64
}

func (op *OperatorDelta) FlushAdaRecord() (record *deltaAdaptationRecord) {
	record = op.AdaptationRecord
	op.AdaptationRecord = new(deltaAdaptationRecord)
	return
}

func (op *OperatorDelta) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]
	deltaVec := original.meta["deltaVec"].([]float64)

	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			mutVec[i] *= deltaVec[i]
			temp := mutVec[i]
			for n := 0; n < op.Norms; n++ {
				mutVec[i] *= rand.NormFloat64() / CONST_SQRT_2_PI
			}

			if mutVec[i] == 0 {
				mutVec[i] = temp * 1.1
			}
		}
	}

	child.AddSurvivalCallback(func(survivor *Individ) {
		parent := survivor.Parent()
		deltaVec := parent.meta["deltaVec"].([]float64)
		childDeltaVec := make([]float64, len(parent.genome))
		for i := range childDeltaVec {
			delta := math.Abs(survivor.genome[i] - parent.genome[i])
			if delta != 0.0 || (op.CrossConvergence && crossVec[i] != 0) {
				adaptation := (delta - deltaVec[i]) / op.Tau
				childDeltaVec[i] = deltaVec[i]*op.ConvergenceBias + adaptation

				// RecordKeeping
				if op.AdaptationRecord != nil {
					record := adaptation / (survivor.blueprint[i].Range * op.mean)
					op.AdaptationRecord.Total += record
					op.AdaptationRecord.TotalAbs += math.Abs(record)
					op.AdaptationRecord.TotalFrac++
					if record > 0 {
						op.AdaptationRecord.TotalPosFrac++
					}
					if delta == mutVec[i] {
						if op.AdaptationRecord != nil {
							// RecordKeeping for adaptation purely caused by mutation
							op.AdaptationRecord.Mut += record
							op.AdaptationRecord.MutAbs += math.Abs(record)
							op.AdaptationRecord.MutFrac++
							if record > 0 {
								op.AdaptationRecord.MutPosFrac++
							}
						}
					} else {
						if mutVec[i] == 0 {
							// RecordKeeping for adaptation purely caused by crossover
							op.AdaptationRecord.Alien += record
							op.AdaptationRecord.AlienAbs += math.Abs(record)
							op.AdaptationRecord.AlienFrac++
							if record > 0 {
								op.AdaptationRecord.AlienPosFrac++
							}
						} else {
							// RecordKeeping for adaptation caused by crossover and mutation together
							op.AdaptationRecord.Combined += record
							op.AdaptationRecord.CombinedAbs += math.Abs(record)
							op.AdaptationRecord.CombinedFrac++
							if record > 0 {
								op.AdaptationRecord.CombinedPosFrac++
							}
						}
					}
				}
			} else {
				childDeltaVec[i] = deltaVec[i]
			}
			if op.AdaptationRecord != nil {
				op.AdaptationRecord.TotalCount++
				op.AdaptationRecord.Value += childDeltaVec[i] / survivor.blueprint[i].Range
			}
		}
		survivor.meta["deltaVec"] = childDeltaVec
	})

	return false
}

func (op *OperatorDelta) Init(ea *EA) {
	op.ea = ea
	for _, individ := range ea.Population {
		deltaVec := vecmat.ZeroVec(len(individ.genome))
		for i := 0; i < len(deltaVec); i++ {
			deltaVec[i] = 0.25 * ea.Blueprint()[i].Range
		}
		individ.meta["deltaVec"] = deltaVec
	}
	op.Update(ea)
}

func (op *OperatorDelta) Update(ea *EA) {
	op.ea = ea
	bp := ea.Blueprint()
	op.mean = 0.0
	for _, individ := range ea.Population {
		deltaVec := individ.meta["deltaVec"].([]float64)
		for i, delta := range deltaVec {
			op.mean += delta / bp[i].Range
		}
	}
	op.mean /= float64(len(ea.Population)) * float64(len(bp))
}

func (op *OperatorDelta) MetaData(ea *EA, individ *Individ, metaData map[string]interface{}) {
	history := make([][]float64, len(individ.ancestors)+1)
	for i, ancestor := range individ.ancestors {
		history[i] = ancestor.meta["deltaVec"].([]float64)
	}
	history[len(history)-1] = individ.meta["deltaVec"].([]float64)

	metaData["delta"] = history
}

func (op *OperatorDelta) MetaStats(metaStats map[string]Stats) {
	metaStats["deltaLength"] = op.ea.Stats(func(individ *Individ) float64 {
		return vecmat.ManhattanLen(individ.meta["delta"].([]float64))
	}, true)
}

func (op *OperatorDelta) MetaGenomeDistribution(distributionStats map[string][]Stats) {
	distributionStats["delta"] = op.ea.StatsArray(func(individ *Individ) []float64 {
		return individ.meta["delta"].([]float64)
	}, true)
}

func (op *OperatorDelta) String() string {
	return fmt.Sprintf("(Operator)\t Delta (Tau: %v, Norms: %v, ConvergenceBias: %v, CrossConvergence: %t)",
		op.Tau, op.Norms, op.ConvergenceBias, op.CrossConvergence)
}

func NewOperatorDelta(tau float64, norms int, convergenceBias float64, crossConverge bool, keepRecord bool) (op *OperatorDelta) {
	op = new(OperatorDelta)
	op.Tau = tau
	op.Norms = norms
	op.ConvergenceBias = convergenceBias
	op.CrossConvergence = crossConverge
	if keepRecord {
		op.AdaptationRecord = new(deltaAdaptationRecord)
	}
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Delta FAKE ------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorDeltaFake struct {
	ea               *EA
	Tau              float64
	ConvergenceBias  float64
	CrossConvergence bool

	AdaptationRecord *deltaAdaptationRecord
}

func (op *OperatorDeltaFake) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	deltaVec := original.meta["mut"].([]float64)

	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			mutVec[i] *= deltaVec[i] * rand.NormFloat64() * rand.NormFloat64() / CONST_2_PI
			for mutVec[i] == 0 {
				mutVec[i] = deltaVec[i] * rand.NormFloat64() * rand.NormFloat64() / CONST_2_PI
			}
		}
	}

	child.AddSurvivalCallback(func(survivor *Individ) {
		childDeltaVec := make([]float64, len(original.genome))
		for i := range childDeltaVec {
			if survivor.genome[i]-original.genome[i] != 0.0 || (op.CrossConvergence && crossVec[i] != 0) {
				childDeltaVec[i] = math.Max(deltaVec[i]*op.ConvergenceBias, math.Pow10(-10)*survivor.blueprint[i].Range)
			} else {
				childDeltaVec[i] = deltaVec[i]
			}
		}
		survivor.meta["mut"] = childDeltaVec
	})

	return false
}

func (op *OperatorDeltaFake) Init(ea *EA) {
	op.ea = ea
	for _, individ := range ea.Population {
		deltaVec := vecmat.ZeroVec(len(individ.genome))
		for i := 0; i < len(deltaVec); i++ {
			deltaVec[i] = 0.25 * ea.Blueprint()[i].Range
		}
		individ.meta["mut"] = deltaVec
	}
}

func (op *OperatorDeltaFake) String() string {
	return fmt.Sprintf("(Operator)\t Delta (Tau: %v, ConvergenceBias: %v, CrossConvergence: %t)",
		op.Tau, op.ConvergenceBias, op.CrossConvergence)
}

func NewOperatorDeltaFake(convergenceBias float64, crossConverge bool) (op *OperatorDeltaFake) {
	op = new(OperatorDeltaFake)
	op.ConvergenceBias = convergenceBias
	op.CrossConvergence = crossConverge
	return
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- DeltaMeanSD --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorDeltaGauss struct {
	ea              *EA
	Tau             float64
	ConvergenceBias float64

	AdaptationRecord *deltaAdaptationRecord
}

func (op *OperatorDeltaGauss) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	deltaVec := original.meta["deltaVec"].([]float64)

	for i := 0; i < len(mutVec); i++ {
		if mutVec[i] != 0 {
			mutVec[i] *= deltaVec[i] + deltaVec[i]*rand.NormFloat64()/(3*CONST_SQRT_2_PI)
			for mutVec[i] == 0 {
				mutVec[i] = deltaVec[i] + deltaVec[i]*rand.NormFloat64()/(3*CONST_SQRT_2_PI)
			}
			if rand.Intn(2) == 1 {
				mutVec[i] *= -1
			}
		}
	}

	child.AddSurvivalCallback(func(survivor *Individ) {
		childDeltaVec := make([]float64, len(original.genome))
		for i := range childDeltaVec {
			var (
				delta      = survivor.genome[i] - original.genome[i]
				adaptation = 0.0
			)
			if delta != 0.0 {
				adaptation = (math.Abs(delta) - deltaVec[i]) / op.Tau
				// RecordKeeping
				if delta == mutVec[i] {
					if op.AdaptationRecord != nil {
						// RecordKeeping for adaptation purely caused by mutation
						op.AdaptationRecord.Mut += adaptation
						op.AdaptationRecord.MutAbs += math.Abs(adaptation)
					}
				} else {
					if op.AdaptationRecord != nil {
						if mutVec[i] == 0 {
							// RecordKeeping for adaptation purely caused by crossover
							op.AdaptationRecord.Alien += adaptation
							op.AdaptationRecord.AlienAbs += math.Abs(adaptation)
						} else {
							// RecordKeeping for adaptation caused by crossover and mutation together
							op.AdaptationRecord.Combined += adaptation
							op.AdaptationRecord.CombinedAbs += math.Abs(adaptation)
						}
					}
				}
			}

			childDeltaVec[i] = deltaVec[i] + adaptation
		}
		survivor.meta["deltaVec"] = childDeltaVec
	})

	return false
}

func (op *OperatorDeltaGauss) FlushAdaRecord() (record *deltaAdaptationRecord) {
	record = op.AdaptationRecord
	op.AdaptationRecord = new(deltaAdaptationRecord)
	return
}

func (op *OperatorDeltaGauss) Init(ea *EA) {
	op.ea = ea
	for _, individ := range ea.Population {
		deltaVec := vecmat.ZeroVec(len(individ.genome))
		for i := 0; i < len(deltaVec); i++ {
			deltaVec[i] = 0.25 * individ.blueprint[i].Range
		}
		individ.meta["deltaVec"] = deltaVec
	}
}

func (op *OperatorDeltaGauss) MetaData(ea *EA, individ *Individ, metaData map[string]interface{}) {
	history := make([][]float64, len(individ.ancestors)+1)
	for i, ancestor := range individ.ancestors {
		history[i] = ancestor.meta["deltaVec"].([]float64)
	}
	history[len(history)-1] = individ.meta["deltaVec"].([]float64)

	metaData["delta"] = history
}

func (op *OperatorDeltaGauss) MetaStats(metaStats map[string]Stats) {
	metaStats["deltaLength"] = op.ea.Stats(func(individ *Individ) float64 {
		return vecmat.ManhattanLen(individ.meta["delta"].([]float64))
	}, true)
}

func (op *OperatorDeltaGauss) MetaGenomeDistribution(distributionStats map[string][]Stats) {
	distributionStats["delta"] = op.ea.StatsArray(func(individ *Individ) []float64 {
		return individ.meta["delta"].([]float64)
	}, true)
}

func (op *OperatorDeltaGauss) String() string {
	return fmt.Sprintf("(Operator)\t Delta (Tau: %v)", op.Tau)
}

func NewOperatorDeltaGauss(tau float64) (op *OperatorDeltaGauss) {
	op = new(OperatorDeltaGauss)
	op.Tau = tau
	op.AdaptationRecord = new(deltaAdaptationRecord)
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Direction -------------------------------------------------
// -------------------------------------------------------------------------------------------------------

// OperatorMinDist sets the expected stepSize of the mutation to be the distance to the parents closest neighboor.
type OperatorDirection struct {
	Tau         float64
	Alpha, Beta float64
}

func (op *OperatorDirection) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	dirVec := op.getDirVec(original)

	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < dirVec[i] {
			mutVec[i] = math.Abs(mutVec[i])
		} else {
			mutVec[i] = -math.Abs(mutVec[i])
		}
	}
	return false
}

func (op *OperatorDirection) getDirVec(original *Individ) []float64 {
	dirVec, ok := original.meta["dirVec"].([]float64)
	if !ok {
		dirVec = make([]float64, len(original.genome))
		grandParent := original.Parent()
		grandDirVec := op.getDirVec(grandParent)
		for i := 0; i < len(dirVec); i++ {
			delta := original.genome[i] - grandParent.genome[i]
			if delta != 0.0 {
				if delta > 0 {
					dirVec[i] = grandDirVec[i] + grandDirVec[i]*(op.Beta-grandDirVec[i])/op.Tau
				} else {
					dirVec[i] = grandDirVec[i] + (1-grandDirVec[i])*(op.Alpha-grandDirVec[i])/op.Tau
				}
			} else {
				dirVec[i] = grandDirVec[i]
			}
		}
		original.meta["dirVec"] = dirVec
	}

	return dirVec
}

func (op *OperatorDirection) Init(ea *EA) {
	for _, individ := range ea.Population {
		dirVec := vecmat.ZeroVec(len(individ.genome))
		for i := 0; i < len(dirVec); i++ {
			dirVec[i] = 0.5
		}
		individ.meta["dirVec"] = dirVec
	}
}

func (op *OperatorDirection) MetaData(ea *EA, individ *Individ, metaData map[string]interface{}) {
	history := make([][]float64, len(individ.ancestors)+1)
	for i, ancestor := range individ.ancestors {
		history[i] = op.getDirVec(ancestor)
	}
	history[len(history)-1] = op.getDirVec(individ)

	metaData["dir"] = history
}

func (op *OperatorDirection) String() string {
	return fmt.Sprintf("(Operator)\t Direction (Tau: %v, Alpha: %v, Beta: %v)", op.Tau, op.Alpha, op.Beta)
}

func NewOperatorDirection(tau, alpha, beta float64) *OperatorDirection {
	dirOp := new(OperatorDirection)
	dirOp.Tau = tau
	dirOp.Alpha = alpha
	dirOp.Beta = beta

	return dirOp
}
