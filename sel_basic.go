package vecea

import (
	"fmt"
	"math"
	"math/rand"
)

type Selector interface {
	Select(ch chan []*Individ, population Population, childCount, parentCount int)
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Roulette --------------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type SelectorRoulette struct{}

func (sel *SelectorRoulette) Select(ch chan []*Individ, population Population, childCount, parentCount int) {

	for i := 0; i < childCount; i++ {
		parents := make(Population, parentCount)

		for p := 0; p < parentCount; p++ {
			parent := population.IndividRoulette()
			for parents.IndexOfIndivid(parent) != -1 {
				parent = population.IndividRoulette()
			}
			parents[p] = parent
		}

		perm := rand.Perm(parentCount)
		permParents := make([]*Individ, parentCount)
		for i, j := range perm {
			permParents[i] = parents[j]
		}

		ch <- parents
	}
	close(ch)
}

func (sel *SelectorRoulette) String() string {
	return fmt.Sprintf("(Selector)\t Roulette")
}

func NewSelectorRoulette() *SelectorRoulette {
	return new(SelectorRoulette)
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------- Roulette Neighbours --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type SelectorRouletteNeighbours struct{}

func (sel *SelectorRouletteNeighbours) Select(ch chan []*Individ, population Population, childCount, parentCount int) {

	for i := 0; i < childCount; i++ {
		parents := make(Population, parentCount)
		parents[0] = population.IndividRoulette()
		for p := 1; p < parentCount; p++ {
			minDist := math.MaxFloat64
			for _, candidate := range population {
				if parents.IndexOfIndivid(candidate) == -1 {
					continue
				}
				dist := 0.0
				for i := 0; i < p; i++ {
					dist += candidate.DistanceManhattanFractionalTo(parents[i])
				}
				if dist < minDist {
					minDist = dist
					parents[p] = candidate
				}
			}
		}

		perm := rand.Perm(parentCount)
		permParents := make([]*Individ, parentCount)
		for i, j := range perm {
			permParents[i] = parents[j]
		}

		ch <- parents
	}
	close(ch)
}

func (sel *SelectorRouletteNeighbours) String() string {
	return fmt.Sprintf("(Selector)\t RouletteNeighbours")
}

func NewSelectorRouletteNeighbours() *SelectorRouletteNeighbours {
	return new(SelectorRouletteNeighbours)
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Incremental -----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type SelectorIncremental struct{}

func (sel *SelectorIncremental) Select(ch chan []*Individ, population Population, childCount, parentCount int) {
	for i, index := 0, 0; i < childCount; i++ {
		index++
		if index == len(population) {
			index = 0
		}
		parents := make(Population, parentCount)

		parents[0] = population[index]
		for p := 1; p < parentCount; p++ {
			parent := population.IndividRand()
			for parents.IndexOfIndivid(parent) != -1 {
				parent = population.IndividRand()
			}
			parents[p] = parent
		}

		ch <- parents
	}
	close(ch)
}

func (sel *SelectorIncremental) String() string {
	return fmt.Sprintf("(Selector)\t Incremental")
}

func NewSelectorIncremental() *SelectorIncremental {
	return new(SelectorIncremental)
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------- Incremental Neighbours -----------------------------------------------
// -------------------------------------------------------------------------------------------------------

type SelectorIncrementalNeighbours struct{}

func (sel *SelectorIncrementalNeighbours) Select(ch chan []*Individ, population Population, childCount, parentCount int) {
	for i, index := 0, 0; i < childCount; i++ {
		index++
		if index == len(population) {
			index = 0
		}
		parents := make(Population, parentCount)

		parents[0] = population[index]
		for p := 1; p < parentCount; p++ {
			minDist := math.MaxFloat64
		candidateLoop:
			for _, candidate := range population {
				dist := 0.0
				for i := 0; i < p; i++ {
					parent := parents[i]
					if parent == candidate {
						continue candidateLoop
					}
					dist += candidate.DistanceManhattanFractionalTo(parent)
				}
				if dist < minDist {
					minDist = dist
					parents[p] = candidate
				}
			}
		}

		ch <- parents
	}
	close(ch)
}

func (sel *SelectorIncrementalNeighbours) String() string {
	return fmt.Sprintf("(Selector)\t Incremental")
}

func NewSelectorIncrementalNeighbours() *SelectorIncrementalNeighbours {
	return new(SelectorIncrementalNeighbours)
}
