package vecea

import (
	"fmt"
	"math"
	"math/rand"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Uniform ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorUniform struct {
	CR float64
}

func (op *OperatorUniform) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(child.genome); i++ {
		if rand.Float64() < op.CR {
			crossVec[i] = 1
		}
	}

	return false
}

func (op *OperatorUniform) ParentDemand() int {
	return 2
}

func (op *OperatorUniform) String() string {
	return fmt.Sprintf("(Operator)\t Uniform (CR: %v)", op.CR)
}

func NewOperatorUniform(crossoverP float64) *OperatorUniform {
	uniform := new(OperatorUniform)
	uniform.CR = crossoverP
	return uniform
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- UniformAdaptive -------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorUniformAdaptive struct{}

func (op *OperatorUniformAdaptive) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	CR := parents[0].meta["CR"].(float64)
	CR = math.Max(math.Min(rand.NormFloat64()*0.1, 1.0), 0.0)

	CR2 := 0.0
	for i := 0; i < len(crossVec); i++ {
		if rand.Float64() < CR {
			crossVec[i] = 1
			CR2++
		}
	}
	CR2 = math.Min(math.Max(CR2, 5)/float64(len(crossVec)), 1.0)

	child.SetMeta("CR", CR2, false)

	return false
}

func (op *OperatorUniformAdaptive) ParentDemand() int {
	return 2
}

func (op *OperatorUniformAdaptive) Init(ea *EA) {
	for _, individ := range ea.Population {
		individ.SetMeta("CR", 0.25, false)
	}
}

func (op *OperatorUniformAdaptive) String() string {
	return fmt.Sprintf("(Operator)\t Uniform Adaptive")
}

func NewOperatorUniformAdaptive() (op *OperatorUniformAdaptive) {
	op = new(OperatorUniformAdaptive)
	return
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------- UniformAdaptive SADE -----------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorUniformAdaptiveSADE struct {
	Max, Min, ChangeRate float64
}

func (op *OperatorUniformAdaptiveSADE) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	CR := op.getRate(parents[0])
	child.SetMeta("CR", CR, false)

	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < CR {
			crossVec[i] = 1
		}
	}

	return false
}

func (op *OperatorUniformAdaptiveSADE) getRate(original *Individ) (CR float64) {
	if rand.Float64() < op.ChangeRate {
		CR = op.Min + rand.Float64()*(op.Max-op.Min)
	} else {
		CR = original.meta["CR"].(float64)
	}
	return
}

func (op *OperatorUniformAdaptiveSADE) ParentDemand() int {
	return 2
}

func (op *OperatorUniformAdaptiveSADE) Init(ea *EA) {
	for _, individ := range ea.Population {
		individ.SetMeta("CR", rand.Float64(), false)
	}
}

func (op *OperatorUniformAdaptiveSADE) String() string {
	return fmt.Sprintf("(Operator)\t Uniform SADE (ChangeRate: %v, Min: %v, Max: %v)",
		op.ChangeRate, op.Min, op.Max)
}

func NewOperatorUniformAdaptiveSADE(changeRate, min, max float64) (op *OperatorUniformAdaptiveSADE) {
	op = new(OperatorUniformAdaptiveSADE)
	op.Max = max
	op.Min = min
	op.ChangeRate = changeRate
	return
}

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- Arithmetic --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorArithmetic struct {
	CR float64
	F  float64
}

func (op *OperatorArithmetic) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(child.genome); i++ {
		if rand.Float64() < op.CR {
			a := rand.Float64() * rand.NormFloat64() * op.F
			child.genome[i] = a*parents[0].genome[i] + (1-a)*parents[1].genome[i]
		}
	}
	return false
}

func (op *OperatorArithmetic) ParentDemand() int {
	return 2
}

func (op *OperatorArithmetic) String() string {
	return fmt.Sprintf("(Operator)\t Arithmetic (CR: %v, F: %v)", op.CR, op.F)
}

func NewOperatorArithmetic(CR, F float64) *OperatorArithmetic {
	ariOp := new(OperatorArithmetic)
	ariOp.CR = CR
	ariOp.F = F
	return ariOp
}

// -------------------------------------------------------------------------------------------------------
// --------------------------------------- Differential --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorDifferential struct {
	CR float64
	F  float64
}

func (op *OperatorDifferential) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < op.CR {
			mutVec[i] = op.F * (parents[1].genome[i] - parents[2].genome[i])
			crossVec[i] = 3
		}
	}
	child.SetMeta("parent", parents[0], false)
	return false
}

func (op *OperatorDifferential) ParentDemand() int {
	return 4
}

func (op *OperatorDifferential) String() string {
	return fmt.Sprintf("(Operator)\t Differential (CR: %v, F: %v)", op.CR, op.F)
}

func NewOperatorDifferential(CR, F float64) (op *OperatorDifferential) {
	op = new(OperatorDifferential)
	op.CR = CR
	op.F = F
	return
}

// -------------------------------------------------------------------------------------------------------
// --------------------------------------- Single point --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorSinglePoint struct{}

func (op *OperatorSinglePoint) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	pointIndex := rand.Intn(len(child.genome))
	for i := pointIndex; i < len(child.genome); i++ {
		crossVec[i] = 1
	}
	return false
}

func (op *OperatorSinglePoint) ParentDemand() int {
	return 2
}

func (op *OperatorSinglePoint) String() string {
	return fmt.Sprint("(Operator)\t SinglePoint")
}

func NewOperatorSinglePoint() *OperatorSinglePoint {
	op := new(OperatorSinglePoint)
	return op
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- Two-point ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorTwoPoint struct {
	Sigma float64
}

func NewOperatorTwoPoint(sigma float64) *OperatorTwoPoint {
	op := new(OperatorTwoPoint)
	op.Sigma = sigma
	return op
}

func (op *OperatorTwoPoint) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {

	firstIndex := rand.Intn(len(child.genome))
	size := int(math.Min(math.Abs(rand.NormFloat64()*op.Sigma/CONST_SQRT_2_PI*float64(len(child.genome))),
		float64(len(child.genome))))
	secondIndex := (firstIndex + size) % len(child.genome)
	for i := firstIndex; i < secondIndex; i = (i + 1) % len(child.genome) {
		crossVec[i] = 1
	}

	return false
}

func (op *OperatorTwoPoint) ParentDemand() int {
	return 2
}

func (op *OperatorTwoPoint) String() string {
	return fmt.Sprint("(Operator)\t TwoPoint")
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Npoint ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorNPoint struct {
	ParentCount int
	CR          float64
}

func (op *OperatorNPoint) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	parentIndex := rand.Intn(len(parents))
	for i := 0; i < len(child.genome); i++ {
		crossVec[i] = parentIndex
		if rand.Float64() < op.CR {
			parentIndex = (parentIndex + 1) % op.ParentCount
		}
	}
	return false
}

func (op *OperatorNPoint) ParentDemand() int {
	return op.ParentCount
}

func (op *OperatorNPoint) String() string {
	return fmt.Sprintf("(Operator)\t NPoint (ParentCount: %v, CR: %v)", op.ParentCount, op.CR)
}

func NewOperatorNPoint(parentCount int, CR float64) (op *OperatorNPoint) {
	op = new(OperatorNPoint)
	op.ParentCount = parentCount
	op.CR = CR
	return
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- Npoint SADE --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorNPointSADE struct {
	ParentCount          int
	Min, Max, ChangeRate float64
	Mean, SD             float64
}

func (op *OperatorNPointSADE) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {

	CR := op.getRate(parents[0])
	child.meta["CR"] = CR

	parentIndex := rand.Intn(len(parents))
	for i := 0; i < len(child.genome); i++ {
		crossVec[i] = parentIndex
		if rand.Float64() < CR {
			parentIndex = (parentIndex + 1) % op.ParentCount
		}
	}
	return false
}

func (op *OperatorNPointSADE) ParentDemand() int {
	return op.ParentCount
}

func (op *OperatorNPointSADE) getRate(original *Individ) (CR float64) {
	if rand.Float64() < op.ChangeRate {
		CR = op.Min + rand.Float64()*(op.Max-op.Min)
	} else {
		CR = original.meta["CR"].(float64)
	}
	return
}

func (op *OperatorNPointSADE) Init(ea *EA) {
	for _, individ := range ea.Population {
		individ.SetMeta("CR", op.Min+rand.Float64()*(op.Max-op.Min), false)
	}
}

func (op *OperatorNPointSADE) String() string {
	return fmt.Sprintf("(Operator)\t NPointSADE (ParentCount: %v, ChangeRate: %v, Min: %v, Max: %v)",
		op.ParentCount, op.ChangeRate, op.Min, op.Max)
}

func NewOperatorNPointSADE(parentCount int, changeRate, min, max float64) (op *OperatorNPointSADE) {
	op = new(OperatorNPointSADE)
	op.ParentCount = parentCount
	op.ChangeRate = changeRate
	op.Min = min
	op.Max = max
	return
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- Npoint ASex --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorNPointASex struct {
	ParentCount          int
	Min, Max, ChangeRate float64
}

func (op *OperatorNPointASex) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {

	CR := op.getRate(parents[0])
	child.meta["CR"] = CR
	if CR == 0.0 {
		return false
	}

	parentIndex := rand.Intn(len(parents))
	for i := 0; i < len(child.genome); i++ {
		crossVec[i] = parentIndex
		if rand.Float64() < CR {
			parentIndex = (parentIndex + 1) % op.ParentCount
		}
	}
	return false
}

func (op *OperatorNPointASex) ParentDemand() int {
	return op.ParentCount
}

func (op *OperatorNPointASex) getRate(original *Individ) (CR float64) {
	if rand.Float64() < op.ChangeRate {
		if rand.Float64() < 0.5 {
			CR = 0.0
		} else {
			CR = op.Min + rand.Float64()*(op.Max-op.Min)
		}
	} else {
		CR = original.meta["CR"].(float64)
	}
	return
}

func (op *OperatorNPointASex) Init(ea *EA) {
	for _, individ := range ea.Population {
		if rand.Float64() < 0.5 {
			individ.SetMeta("CR", 0.0, false)
		} else {
			individ.SetMeta("CR", op.Min+rand.Float64()*(op.Max-op.Min), false)
		}
	}
}

func (op *OperatorNPointASex) String() string {
	return fmt.Sprintf("(Operator)\t NPointASex (ParentCount: %v, ChangeRate: %v, Min: %v, Max: %v)",
		op.ParentCount, op.ChangeRate, op.Min, op.Max)
}

func NewOperatorNPointASex(parentCount int, changeRate, min, max float64) (op *OperatorNPointASex) {
	op = new(OperatorNPointASex)
	op.ParentCount = parentCount
	op.ChangeRate = changeRate
	op.Min = min
	op.Max = max
	return
}

// -------------------------------------------------------------------------------------------------------
// --------------------------------------- Uniform Rate --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OP_Uniform_Rate struct{}

func (op *OP_Uniform_Rate) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	pIndex := 1
	// if rand.Float64() < 0.5 {
	// 	pIndex = 0
	// 	for i := range crossVec {
	// 		crossVec[i] = 1
	// 	}
	// }

	for i := 0; i < len(child.genome); i++ {
		if mutVec[i] != 0 {
			crossVec[i] = pIndex
		}
	}
	return false
}

func (op *OP_Uniform_Rate) ParentDemand() int {
	return 2
}

func (op *OP_Uniform_Rate) String() string {
	return fmt.Sprintf("(Operator)\t Uniform Rate")
}

func NewOP_Uniform_Rate() (op *OP_Uniform_Rate) {
	op = new(OP_Uniform_Rate)
	return
}
