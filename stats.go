package vecea

import "math"

type Stats struct {
	Mean float64 `json:"e"`
	Min  float64 `json:"i"`
	Max  float64 `json:"a"`
	SD   float64 `json:"s"`
}

func NewStats(vals []float64, sd ...bool) (stats Stats) {
	// Make sure min is recorded
	stats.Min = math.MaxFloat64
	stats.Max = -math.MaxFloat64

	// Mean, Min, Max
	for _, val := range vals {
		stats.Mean += val
		stats.Min = math.Min(stats.Min, val)
		stats.Max = math.Max(stats.Max, val)
	}
	stats.Mean /= float64(len(vals))

	// SD
	if len(sd) == 0 || sd[0] {
		for _, val := range vals {
			stats.SD += math.Pow(val-stats.Mean, 2)
		}
		stats.SD = math.Sqrt(stats.SD / float64(len(vals)))
	} else {
		stats.SD = -1
	}
	return
}
