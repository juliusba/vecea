package vecea

type MetaData interface {
	MetaData(ea *EA, individ *Individ, metaData map[string]interface{})
}

type IndividStats struct {
	TestScores    map[string]float64     `json:"sco"`
	GenomeHistory [][]float64            `json:"gen"`
	MetaData      map[string]interface{} `json:"met"`
}

func (ea *EA) StatsIndivid(individ *Individ) (stats *IndividStats) {
	stats = new(IndividStats)

	stats.TestScores = individ.testScores

	stats.GenomeHistory = make([][]float64, len(individ.ancestors)+1)
	stats.GenomeHistory[len(stats.GenomeHistory)-1] = individ.genome
	for i, ancestor := range individ.ancestors {
		stats.GenomeHistory[i] = ancestor.genome
	}

	stats.MetaData = make(map[string]interface{})
	for _, comp := range ea.Components {
		if comp, ok := comp.(MetaData); ok {
			comp.MetaData(ea, individ, stats.MetaData)
		}
	}

	return stats
}
