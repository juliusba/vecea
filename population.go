package vecea

import (
	"fmt"
	"math/rand"
	"sort"
	"sync"
)

type Population []*Individ

func (pop Population) IndividBest() *Individ {
	if !sort.IsSorted(pop) {
		sort.Sort(pop)
	}
	return pop[0]
}

func (pop Population) IndividRoulette() *Individ {
	ticket := rand.Float64()
	for _, individ := range pop {
		ticket -= individ.ExpVal()
		if ticket <= 0 {
			return individ
		}
	}
	fmt.Println("ERROR (vecea.Population.Roulette): Populations expvals is not normalized! (Sum is less than 1...")
	return pop.IndividRand()
}

func (pop Population) IndividRand() *Individ {
	return pop[rand.Intn(len(pop))]
}

func (pop Population) IndexOfIndivid(individ *Individ) int {
	for i, tIndivid := range pop {
		if tIndivid == individ {
			return i
		}
	}

	return -1
}

func (pop Population) DoAll(do func(individ *Individ)) {
	var wg sync.WaitGroup

	wg.Add(len(pop))
	for _, individ := range pop {
		go func(individ *Individ) {
			defer wg.Done()
			do(individ)
		}(individ)
	}

	wg.Wait()
}

func (pop Population) DoAllIndexes(do func(individ *Individ, index int)) {
	var wg sync.WaitGroup

	wg.Add(len(pop))
	for index, individ := range pop {
		go func(individ *Individ, index int) {
			defer wg.Done()
			do(individ, index)
		}(individ, index)
	}

	wg.Wait()
}

func (pop Population) DoAllWithLock(rwLock *sync.RWMutex, do func(individ *Individ, rwLock *sync.RWMutex)) {
	var wg sync.WaitGroup
	if rwLock == nil {
		rwLock = new(sync.RWMutex)
	}

	wg.Add(len(pop))
	for _, individ := range pop {
		go func(individ *Individ) {
			defer wg.Done()
			do(individ, rwLock)
		}(individ)
	}

	wg.Wait()
}

func (pop Population) DoAllIndexesWithLock(rwLock *sync.RWMutex, do func(individ *Individ, index int, rwLock *sync.RWMutex)) {
	var wg sync.WaitGroup
	if rwLock == nil {
		rwLock = new(sync.RWMutex)
	}

	wg.Add(len(pop))
	for index, individ := range pop {
		go func(individ *Individ, index int) {
			defer wg.Done()
			do(individ, index, rwLock)
		}(individ, index)
	}

	wg.Wait()
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------ Sort by fitness --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (pop Population) Len() int {
	return len(pop)
}

func (pop Population) Less(i, j int) bool {
	if pop[i].fitness == pop[j].fitness {
		return pop[i].age < pop[j].age
	}
	return pop[i].fitness < pop[j].fitness
}

func (pop Population) Swap(i, j int) {
	temp := pop[i]
	pop[i] = pop[j]
	pop[j] = temp
}

func (pop Population) Sort() {
	sort.Sort(pop)
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------ Sort by expVal ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type _expValSort []*Individ

func (pop _expValSort) Len() int {
	return len(pop)
}

func (pop _expValSort) Less(i, j int) bool {
	if pop[i].expVal == pop[j].expVal {
		return pop[i].age > pop[j].age
	}
	return pop[i].expVal > pop[j].expVal
}

func (pop _expValSort) Swap(i, j int) {
	temp := pop[i]
	pop[i] = pop[j]
	pop[j] = temp
}

func (pop Population) SortExpVal() {
	sort.Sort(_expValSort(pop))
}
