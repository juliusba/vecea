package vecea

import (
	"fmt"
	"math"
	"math/rand"
	"vecmat"
)

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- Adaptive ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorRateAdaptive struct{}

func (op *OperatorRateAdaptive) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	parentRate := original.meta["rate"].(float64)
	parentRate = math.Min(parentRate+rand.NormFloat64()*0.1, 1.0)
	rate := 0.0
	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < parentRate {
			mutVec[i] = 1.0
			rate++
		}
	}
	if rate == 0 {
		mutVec[rand.Intn(len(mutVec))] = 1.0
	}
	rate = math.Max(rate, 2.5) / float64(len(mutVec))
	child.meta["rate"] = math.Min(rate, 1.0)

	return false
}

func (op *OperatorRateAdaptive) Init(ea *EA) {
	for _, individ := range ea.Population {
		individ.meta["rate"] = 0.25
	}
}

func (op *OperatorRateAdaptive) String() string {
	return fmt.Sprintf("(Operator)\t RateIndividAdaptive")
}

func NewOperatorRateAdaptive() *OperatorRateAdaptive {
	op := new(OperatorRateAdaptive)
	return op
}

// -------------------------------------------------------------------------------------------------------
// --------------------------------------- GeneAdaptive --------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorRateGeneAdaptive struct {
	Rate        float64
	Tau         float64
	Alpha, Beta float64
}

func (op *OperatorRateGeneAdaptive) ApplyOperator(ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	original := parents[0]

	parentRate := op.getRateVec(original)
	childRateVec := make([]float64, len(parentRate))
	childRate := 0.0
	rate := 0
	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < parentRate[i] {
			mutVec[i] = 1.0
			childRateVec[i] = parentRate[i] + parentRate[i]*(op.Beta-parentRate[i])/op.Tau
			rate++
		} else {
			childRateVec[i] = parentRate[i] + (1-parentRate[i])*(op.Alpha-parentRate[i])/op.Tau
		}
		childRate += childRateVec[i]
	}
	childRate /= float64(len(childRateVec))
	if op.Rate != 0 && op.Rate > childRate {
		for i := 0; i < len(childRateVec); i++ {
			childRateVec[i] *= (childRate / op.Rate)
		}
	}
	child.meta["rateVec"] = childRateVec

	return false
}

func (op *OperatorRateGeneAdaptive) getRateVec(individ *Individ) []float64 {
	rateVec, ok := individ.meta["rateVec"].([]float64)
	if !ok {
		return op.getRateVec(individ.Parent())
	}
	return rateVec
}

func (op *OperatorRateGeneAdaptive) Init(ea *EA) {
	for _, individ := range ea.Population {
		individ.meta["rateVec"] = vecmat.RandVec(len(individ.genome))
	}
}

func (op *OperatorRateGeneAdaptive) String() string {
	return fmt.Sprintf("(Operator)\t RateGeneAdaptive (Tau: %v, Alpha: %v, Beta: %v, Rate: %v)",
		op.Tau, op.Alpha, op.Beta, op.Rate)
}

func NewOperatorRateGeneAdaptive(tau, alpha, beta, rate float64) *OperatorRateGeneAdaptive {
	op := new(OperatorRateGeneAdaptive)
	op.Tau = tau
	op.Alpha = alpha
	op.Beta = beta
	op.Rate = rate

	return op
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------  SADE inspired ------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type OperatorRateSADE struct {
	ChangeRate float64
	Min, Max   float64

	ea *EA
}

var omg int

func (op *OperatorRateSADE) ApplyOperator(
	ea *EA, parents []*Individ, child *Individ, mutVec []float64, crossVec []int) bool {
	rate := op.getRate(parents[0])
	child.meta["rate"] = rate
	for i := 0; i < len(mutVec); i++ {
		if rand.Float64() < rate {
			mutVec[i] = 1.0
		}
	}

	return false
}

func (op *OperatorRateSADE) getRate(original *Individ) (rate float64) {
	if rand.Float64() < op.ChangeRate {
		rate = op.Min + rand.Float64()*(op.Max-op.Min)
	} else {
		rate = original.meta["rate"].(float64)
		_globalRate += rate
		_globalCount++
	}
	return
}

var (
	_globalRate  float64
	_globalCount int
)

func (op *OperatorRateSADE) Init(ea *EA) {
	op.ea = ea
	for _, individ := range ea.Population {
		individ.meta["rate"] = op.Min + rand.Float64()*(op.Max-op.Min)
	}
}

func (op *OperatorRateSADE) MetaStats(metaStats map[string]Stats) {
	metaStats["rate"] = op.ea.Stats(func(individ *Individ) float64 {
		return individ.meta["rate"].(float64)
	}, true)
}

func (op *OperatorRateSADE) String() string {
	return fmt.Sprintf("(Operator)\t RateIndividAdaptiveSADE (ChangeRate: %v, Min: %v, Max: %v)",
		op.ChangeRate, op.Min, op.Max)
}

func NewOperatorRateSADE(changeRate, min, max float64) (op *OperatorRateSADE) {
	op = new(OperatorRateSADE)
	op.ChangeRate = changeRate
	op.Min = min
	op.Max = max
	return
}
