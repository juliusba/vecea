package vecea

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"sync"
	"time"
)

type PopulationCreator interface {
	CreatePopulation(ea *EA, size int) Population
}

type TesterFace interface {
	TestName() (name string)
}
type Finisher interface {
	IsFinished(individ *Individ) (isFinished bool)
}
type PopulationTester interface {
	TesterFace
	TestPopulation(ea *EA) (scores []float64)
}
type IndividTester interface {
	TesterFace
	TestIndivid(individ *Individ, phenotype interface{}) (score float64)
}
type SolutionTester interface {
	TesterFace
	TestSolution(solution interface{}) (score float64)
}
type Tester struct {
	TesterFace
	Weight float64
	Stats
}

type EAiniter interface {
	Init(ea *EA)
}
type EAupdater interface {
	Update(ea *EA)
}
type ParentDemander interface {
	ParentDemand() int
}
type AncestorDemander interface {
	AncestorDemand() int
}

type EA struct {
	Population

	generation int

	generationSize int
	elitism        float64

	ancestorBound   int
	goal            float64
	stagnationBound int
	stagnationSince int

	BP Blueprint

	children Population

	Blueprinter
	PopulationCreator

	ExpValSetter
	Selector
	Executioner

	Operators []Operator

	populationTesters []PopulationTester
	individTesters    []IndividTester
	solutionTesters   []SolutionTester
	Testers           map[string]*Tester

	Components    []interface{}
	distanceStats struct {
		min        Stats
		max        Stats
		mean       Stats
		lastUpdate int
	}

	wg sync.WaitGroup
}

func NewEA(
	generationSize int,
	elitism float64,
	ancestorBound int,
	stagnationBound int,

	populationCreator PopulationCreator,
	blueprinter Blueprinter,
	expValSetter ExpValSetter,
	selector Selector,
	executioner Executioner,

	operators ...Operator,
) *EA {

	rand.Seed(time.Now().UTC().UnixNano())
	ea := new(EA)

	ea.generationSize = generationSize
	ea.elitism = elitism
	ea.ancestorBound = ancestorBound
	ea.stagnationBound = stagnationBound

	ea.PopulationCreator = populationCreator
	ea.Blueprinter = blueprinter
	ea.ExpValSetter = expValSetter
	ea.Selector = selector
	ea.Executioner = executioner

	ea.Operators = operators
	ea.populationTesters = make([]PopulationTester, 0)
	ea.individTesters = make([]IndividTester, 0)
	ea.solutionTesters = make([]SolutionTester, 0)
	ea.Testers = make(map[string]*Tester)

	return ea
}

func (ea *EA) Init() {

	// Test that necessary components are in place
	if ea.Blueprinter == nil {
		log.Fatal("Error (vecea.EA.Init): No blueprint given!")
	}
	ea.BP = ea.Blueprint()
	if len(ea.Testers) == 0 {
		log.Fatal("Error (vecea.EA.Init): No testers given!")
	}

	// Set unassigned components to defaults
	if ea.PopulationCreator == nil {
		ea.PopulationCreator = ea
	}
	if ea.ExpValSetter == nil {
		ea.ExpValSetter = ea
	}
	if ea.Operators == nil {
		ea.Operators = []Operator{
			NewOperatorRate(0.5),
			NewOperatorGauss(),
			NewOperatorScaleRange(0.2, true),
		}
	}
	if ea.Selector == nil {
		ea.Selector = NewSelectorRoulette()
	}
	if ea.Executioner == nil {
		ea.Executioner = NewExecutionerStandard(ea.generationSize)
	}
	if ea.goal == 0 {
		ea.goal = math.MaxFloat64
	}

	// Init population
	ea.Population = ea.PopulationCreator.CreatePopulation(ea, ea.generationSize)

	// Create components slice
	ea.Components = []interface{}{
		ea.Blueprinter,
		ea.PopulationCreator,
		ea.ExpValSetter,
		ea.Selector,
		ea.Executioner,
	}
	for _, op := range ea.Operators {
		ea.Components = append(ea.Components, op)
	}
	for _, tester := range ea.Testers {
		ea.Components = append(ea.Components, tester.TesterFace)
	}

	// Print and initialize components to check for compatibility
	fmt.Println()
	for _, component := range ea.Components {
		if stringer, ok := component.(fmt.Stringer); ok {
			fmt.Println(stringer)
		}
		if initer, ok := component.(EAiniter); ok {
			initer.Init(ea)
		}
	}
	fmt.Println()

	// Test population
	for _, tester := range ea.populationTesters {
		tester.TestPopulation(ea)
	}
	ea.wg.Add(len(ea.Population))
	for _, individ := range ea.Population {
		individ.Phenotype = ea.Phenotype(individ)
		go ea.testIndivid(individ)
	}
	ea.wg.Wait()

	// Set expected percentage of childpool owned by each individual.
	ea.ExpValSetter.SetExpVals(ea)

	// Bound ancestors to what's needed by the given components or specified by user.
	if ea.ancestorBound == 0 {
		ea.ancestorBound = 1
	}
	for _, comp := range ea.Components {
		if demander, ok := comp.(AncestorDemander); ok {
			if demand := demander.AncestorDemand(); demand > ea.ancestorBound {
				ea.ancestorBound = demand
			}
		}
	}
	for _, individ := range ea.Population {
		individ.SetAncestorBound(ea.ancestorBound)
	}
}

func (ea *EA) parentDemand() (parentDemand int) {
	parentDemand = 1
	for _, op := range ea.Operators {
		if demander, ok := op.(ParentDemander); ok {
			if demand := demander.ParentDemand(); demand > parentDemand {
				parentDemand = demand
			}
		}
	}
	return
}

func (ea *EA) RunGenerationStep() (done bool) {
	ea.generation++
	best := ea.IndividBest()

	// Initialize
	ea.children = ea.children[0:0]
	ea.wg.Add(ea.generationSize)

	// Set expected percentage of childpool owned by each individual.
	ea.ExpValSetter.SetExpVals(ea)

	// Select Parents
	parentChannel := make(chan []*Individ, ea.generationSize)
	ea.Selector.Select(parentChannel, ea.Population, ea.generationSize, ea.parentDemand())

	// Create a child for each set of parents
	for parents := range parentChannel {
		child := ea.reproduce(parents)
		ea.children = append(ea.children, child)
	}

	// If elitism is less than one, kill the least fit parents
	if ea.elitism < 1.0 {
		if ea.elitism == 0 {
			ea.Population = ea.Population[:0]
		} else {
			ea.Population.Sort()
			elitists := int(math.Max(ea.elitism*float64(len(ea.Population)), 1))
			ea.Population = ea.Population[:elitists]
		}
	}

	// Wait for fitness tests to finish
	ea.wg.Wait()

	// Append children to the population
	ea.Population = append(ea.Population, ea.children...)

	// Run population testers
	for _, tester := range ea.populationTesters {
		tester.TestPopulation(ea)
	}

	// Set fitness
	ea.setFitness()

	// Kill individs that do not make the cut
	ea.Executioner.Execute(ea)
	for _, individ := range ea.Population {
		individ.IncAge()
	}

	// Tell relevant components that a generation has passed
	for _, comp := range ea.Components {
		if updater, ok := comp.(EAupdater); ok {
			updater.Update(ea)
		}
	}

	// Keep track of number of generations without improvement
	if ea.IndividBest() == best {
		ea.stagnationSince++
	} else {
		ea.stagnationSince = 0
	}

	done = ea.isFinished()
	return
}

func (ea *EA) reproduce(parents []*Individ) (child *Individ) {
	// Initialize
	child = NewIndivid(ea.BP)
	mutVec := make([]float64, ea.GenomeLen())
	crossVec := make([]int, ea.GenomeLen())
	child.genome = make([]float64, ea.GenomeLen())

	// Apply operators
	for _, op := range ea.Operators {
		if op.ApplyOperator(ea, parents, child, mutVec, crossVec) {
			break
		}
	}
	for i := 0; i < len(child.genome); i++ {
		child.genome[i] = parents[crossVec[i]].genome[i] + mutVec[i]
	}

	// Make sure that the individual gene values remain within their domain
	child.BoundGenome()

	// Set parent of child
	child.SetParent(parents[0])

	// Set birthGeneration
	child.birthGeneration = ea.generation

	// Test that the child is in fact not just an exact replica of its parent
	if child.IdenticalTo(child.Parent()) {
		// log.Fatal("Parent and child are identical to one another!")
	}

	// Create phenotype
	child.Phenotype = ea.Phenotype(child)

	// Run tests on child
	go ea.testIndivid(child)

	return
}

func (ea *EA) testIndivid(individ *Individ) {
	solution := individ.Phenotype

	// Run IndividTesters
	for _, tester := range ea.individTesters {
		score := tester.TestIndivid(individ, solution)
		individ.testScores[tester.TestName()] = score
	}

	// Run SolutionTesters
	for _, tester := range ea.solutionTesters {
		score := tester.TestSolution(solution)
		individ.testScores[tester.TestName()] = score
	}

	ea.wg.Done()
}

func (ea *EA) setFitness() {
	// If there is only one tester, set fitness equal to the score on this test and return.
	if len(ea.Testers) == 1 {
		var testName string
		for name := range ea.Testers {
			testName = name
		}
		for _, individ := range ea.Population {
			individ.SetFitness(individ.testScores[testName])
		}
		return
	}

	// Find multiObjective fitness...

	// Find mean for all tests
	for _, individ := range ea.Population {
		for testName := range ea.Testers {
			ea.Testers[testName].Mean += individ.testScores[testName]
			if individ.testScores[testName] > ea.Testers[testName].Max {
				ea.Testers[testName].Max = individ.testScores[testName]
			} else if individ.testScores[testName] < ea.Testers[testName].Min {
				ea.Testers[testName].Min = individ.testScores[testName]
			}
		}
	}
	for testName := range ea.Testers {
		ea.Testers[testName].Mean /= float64(len(ea.Population))
	}

	// Find sd for all tests
	for _, individ := range ea.Population {
		for testName := range ea.Testers {
			temp := individ.testScores[testName] - ea.Testers[testName].Mean
			ea.Testers[testName].SD += temp * temp
		}
	}
	for testName := range ea.Testers {
		ea.Testers[testName].SD = math.Sqrt(ea.Testers[testName].SD / float64(len(ea.Population)))
	}

	// Set fitness for each individual
	for _, individ := range ea.Population {
		fitness := 0.0
		for testName, tester := range ea.Testers {
			fitness += tester.Weight * (individ.testScores[testName] - ea.Testers[testName].Mean) / ea.Testers[testName].SD
		}
		individ.SetFitness(fitness)
	}
}

func (ea *EA) isFinished() (finished bool) {
	if ea.IndividBest().fitness == -math.MaxFloat64 {
		return true
	}
	if ea.stagnationBound > 0 && ea.stagnationSince >= ea.stagnationBound {
		return true
	}
	for _, tester := range ea.Testers {
		if finisher, ok := tester.TesterFace.(Finisher); ok {
			if !finisher.IsFinished(ea.IndividBest()) {
				return false
			}
			finished = true
		}
	}
	return
}

func (_ *EA) CreatePopulation(ea *EA, size int) Population {
	population := make(Population, size)
	for i := 0; i < size; i++ {
		individ := NewIndivid(ea.Blueprinter.Blueprint())
		individ.Randomize()
		population[i] = individ
	}
	return population
}

func (_ *EA) SetExpVals(ea *EA) {
	sum := 0.0
	for _, individ := range ea.Population {
		// In case of negative fitness (TODO: Clean up and make it idiot-proof perhaps)
		if individ.fitness > 0 {
			sum += individ.fitness
		}
	}
	for _, individ := range ea.Population {
		individ.expVal = individ.fitness / math.Abs(sum)
	}
}

func (ea *EA) SetGoal(goal float64) {
	ea.goal = goal
}

func (ea *EA) AddTesters(testers ...interface{}) {
	var prevTesterName string
	for _, tester := range testers {
		switch tester := tester.(type) {
		default:
			log.Fatalf("ERROR (vecea.EA.AddTesters): Trying to add invalid tester (%v)", tester)
		case TesterFace:
			name := tester.(interface {
				TestName() string
			}).TestName()

			// Test if the name is used
			if _, ok := ea.Testers[name]; ok {
				log.Fatalf("ERROR (vecea.EA.AddTesters): Two testers have the same name: %s", name)
			}

			ea.Testers[name] = &Tester{
				tester,
				1.0,
				Stats{},
			}
			prevTesterName = name
		case float64:
			if prevTesterName == "" {
				log.Fatal("ERROR (vecea.EA.AddTesters): Weight must be sent after a valid tester")
			}

			prevTester := ea.Testers[prevTesterName]
			prevTester.Weight = tester
			ea.Testers[prevTesterName] = prevTester
		}

		// Add tester to appropriate array
		switch tester := tester.(type) {
		case PopulationTester:
			ea.populationTesters = append(ea.populationTesters, tester)
		case IndividTester:
			ea.individTesters = append(ea.individTesters, tester)
			fmt.Println("ADDING Individ TESTER")
		case SolutionTester:
			ea.solutionTesters = append(ea.solutionTesters, tester)
		}
	}
}
