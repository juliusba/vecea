package vecea

import (
	"fmt"
	"math"
	"runtime"
	"testing"
	"vecmat"
)

const (
	TESTS           = 5
	QUIT_AFTER      = 10000
	GENERATIONS     = 5000
	GENERATION_SIZE = 100
	DIMENSIONS      = 20
	TRANSLATION     = false
	ROTATION        = false

	RAF_SEGMENTS = 8

	PRINT_RESULTS = true
)

var (
	GOAL               = math.Pow10(-10)
	TESTER Blueprinter = NewTestFunction(NewTFCrossInTray(), DIMENSIONS, GOAL, TRANSLATION, ROTATION)
	// TESTER Blueprinter = NewRobotArmFunc(RAF_SEGMENTS)
)

func testEA(ea *EA) float64 {
	runtime.GOMAXPROCS(runtime.NumCPU())
	runtime.GC()

	ea.AddTesters(TESTER)
	ea.Blueprinter = TESTER
	ea.Init()

	var (
		adaptationRecords          []*deltaAdaptationRecord
		identicalRecords           []float64
		distMean, distMin, distMax Stats
		relMean, relMin, relMax    Stats
		unique, shared, spread, sd Stats
	)

	memStats := new(runtime.MemStats)
	fmt.Printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
	for i, done := 0, false; i < GENERATIONS && !done; i++ {
		done = ea.RunGenerationStep()
		if i%100 == 0 {
			runtime.ReadMemStats(memStats)
			distMean, distMin, distMax = ea.StatsDistance()
			relMean, relMin, relMax = ea.StatsRelation()
			unique = ea.StatsGeneUniqueness()
			shared = ea.StatsGeneSharing()
			spread, sd = ea.StatsGeneSpread()
		}
		fmt.Printf("\r\033[15A")
		fmt.Printf("Gen: \t\t %v         \n", ea.generation)
		fmt.Printf("Best: \t\t %v        \n", ea.IndividBest().fitness)
		fmt.Printf("AvgAge: \t %v        \n", ea.StatsAge())
		fmt.Printf("MeanDist: \t %v        \n", distMean)
		fmt.Printf("MinDist: \t %v        \n", distMin)
		fmt.Printf("MaxDist: \t %v        \n", distMax)
		fmt.Printf("GeneUnique: \t %v        \n", unique)
		fmt.Printf("GeneShared: \t %v        \n", shared)
		fmt.Printf("GeneSpread: \t %v        \n", spread)
		fmt.Printf("GeneSD: \t %v        \n", sd)
		fmt.Printf("MeanRelation: \t %v        \n", relMean)
		fmt.Printf("MinRelation: \t %v        \n", relMin)
		fmt.Printf("MaxRelation: \t %v        \n", relMax)
		fmt.Printf("PopSize: \t %v       \n", len(ea.Population))
		fmt.Printf("Memory(MB): \t %v    \n", memStats.Alloc/1048576)

		if i%200 == 0 {
			for _, op := range ea.Operators {
				if delta, ok := op.(*OperatorDelta); ok {
					if delta.AdaptationRecord != nil {
						adaptationRecords = append(adaptationRecords,
							delta.FlushAdaRecord())
					}
				}
			}

			var identicalCount float64
			totalCount := math.Pow(float64(len(ea.Population)), 2) * float64(len(ea.Population[0].genome))
			for _, one := range ea.Population {
				for _, other := range ea.Population {
					if one == other {
						continue
					}
					for i := range one.genome {
						if one.genome[i] == other.genome[i] {
							identicalCount++
						}
					}
				}
			}
			identicalRecords = append(identicalRecords, identicalCount/totalCount)
		}
	}

	// log.Fatal("Global Rate:", _globalRate/float64(_globalCount))

	if PRINT_RESULTS {
		switch t := TESTER.(type) {
		case *RobotArmFunc:
			fmt.Println()
			fmt.Println("Best:\t ", ea.IndividBest().genome)
		case *TestFunction:
			fmt.Println()
			fmt.Println("Best:\t ", ea.IndividBest().genome)
			if DIMENSIONS < 30 {
				fmt.Println()
				fmt.Println("Best:\t ", ea.IndividBest().genome)
				if ROTATION {
					if TRANSLATION {
						invRot := vecmat.Transpose(t.rotationMatrix)
						origo := vecmat.Invert(vecmat.Rotate(t.translationVector, invRot))
						fmt.Println("Origo:\t ", origo)
						fmt.Println("OnesVec: ", vecmat.SumRows(t.rotationMatrix))
						fmt.Println()
					} else {
						fmt.Println("Origo:\t ", vecmat.ZeroVec(DIMENSIONS))
						fmt.Println("OnesVec: ", vecmat.SumRows(t.rotationMatrix))
						fmt.Println()
					}
				} else if TRANSLATION {
					fmt.Println("Origo:\t ", t.translationVector)
					fmt.Println("OnesVec: ", vecmat.OnesVec(DIMENSIONS))
					fmt.Println()
				}
			}
		}
	}

	for i := range adaptationRecords {
		fmt.Println(i*200, "mutOverCross", adaptationRecords[i].MutAbs/adaptationRecords[i].AlienAbs,
			"MUT Sum over Abs Sum:", adaptationRecords[i].Mut/adaptationRecords[i].MutAbs,
			"Alien Sum over Abs Sum:", adaptationRecords[i].Alien/adaptationRecords[i].AlienAbs,
			"Overall Sum over Abs Sum:", (adaptationRecords[i].Mut+adaptationRecords[i].Alien)/
				(adaptationRecords[i].MutAbs+adaptationRecords[i].AlienAbs),
			"Mut SoAS Minus Alien SoAS:", adaptationRecords[i].Mut/adaptationRecords[i].MutAbs-
				adaptationRecords[i].Alien/adaptationRecords[i].AlienAbs)
	}

	// for i, record := range identicalRecords {
	// 	fmt.Println(i*200, "IdenticalRecord:", record)
	// }

	return ea.IndividBest().fitness
}

// func TestLOL(t *testing.T) {
// 	sum, sd := 0.0, 0.0
// 	c := 0.0
// 	for i := 0.0; i < math.Pow10(7); i++ {
// 		// val := math.Abs(rand.NormFloat64() * rand.NormFloat64() * rand.NormFloat64() * rand.NormFloat64() /
// 		// (CONST_2_PI * CONST_2_PI))
// 		val := math.Abs(rand.NormFloat64() * rand.NormFloat64() / (CONST_2_PI))
// 		// val := math.Abs(rand.NormFloat64() / CONST_SQRT_2_PI)
// 		sum += val
// 		if val <= 1 {
// 			c++
// 		}
// 		sd += math.Pow(val-1, 2)
// 	}
// 	mean := sum / math.Pow10(7)
// 	sd = sd / math.Pow10(7)
// 	c /= math.Pow10(7)
// 	log.Fatal("mean:", mean, " sd:", sd, " pLow:", c)
// }

func TestMixed(t *testing.T) {
	fmt.Println("Mixed...")

	expValSetter := NewExpValRank(4)
	selector := NewSelectorRoulette()
	operators := []Operator{
		NewOperatorRate(0.1),
		// NewOperatorRateNPoint(0.1),
		// NewOperatorRateIndividAdaptive(),
		// NewOperatorRateSADE(0.25, 0.1, 0.5),
		// NewOperatorRateGeneAdaptive(tau, 0.1, 1.0, 0.25),
		// NewOperatorGauss(),
		NewOperatorDelta(1.5, 2, 1.0, false, false),
		// NewOperatorDeltaFake(0.936, false),
		// NewOperatorDeltaDist(0.8),
		// NewOperatorSADE(0.1, 0.5, 1.0, 0.1, 0.0, 1.0),
		// NewOperatorUniformAdaptive(),
		// NewOperatorUniformAdaptiveSADE(0.25, 0.0, 1.0),
		// NewOperatorUniform(0.5),
		// NewOperatorSinglePoint(),
		// NewOperatorTwoPoint(RATE),
		NewOperatorNPoint(2, 0.25),
		// NewOP_Uniform_Rate(),
		// NewOperatorNPointSADE(2, 0.25, 0.0, 0.5),
		// NewOperatorNPointASex(2, 0.25, 0.0, 0.5),
	}
	// executioner := NewExecutionerDistance2(0.01, 1.0, true, true, true)
	// executioner := NewExecutionerRankDistHybrid(0.5, 1.0, true, true, true)
	// executioner := NewExecutionerRankDist(0.5, 0.5, true, true, true)
	executioner := NewExecutionerNeighbourKiller(0.5)
	// executioner := NewExecutionerNeighbourKillerFake(0.5)
	// executioner := NewExecutionerNeighbourTournament(0.5)
	// executioner := NewExecutionerStandard(50)

	ea := NewEA(GENERATION_SIZE, 1.0, 0, QUIT_AFTER, nil, nil, expValSetter, selector, executioner, operators...)
	testEA(ea)
	fmt.Println()
}

// func TestDifferential(t *testing.T) {
// 	fmt.Println("Differential...")

// 	selector := NewSelectorIncremental()
// 	operators := []Operator{
// 		NewOperatorDifferential(0.9, 0.5),
// 	}
// 	executioner := NewExecutionerOedipus()

// 	ea := NewEA(GENERATION_SIZE, 1.0, 0, QUIT_AFTER, nil, nil, nil, selector, executioner, operators...)
// 	testEA(ea)
// 	fmt.Println()
// }

func TestSADE(t *testing.T) {
	fmt.Println("Self-adaptive DE...")

	selector := NewSelectorIncremental()
	operators := []Operator{
		NewOperatorSADE(0.1, 0.1, 1.0, 0.1, 0.0, 1.0),
	}
	executioner := NewExecutionerOedipus()

	ea := NewEA(GENERATION_SIZE, 1.0, 0, QUIT_AFTER, nil, nil, nil, selector, executioner, operators...)
	testEA(ea)
	fmt.Println()
}

func TestStandard(t *testing.T) {
	fmt.Println("Standard...")

	expValSetter := NewExpValRank(4)
	selector := NewSelectorRoulette()
	operators := []Operator{
		NewOperatorRate(0.1),
		// NewOperatorPower(),
		NewOperatorGauss(),
		NewOperatorScaleRange(0.2, true),
		NewOperatorSinglePoint(),
		// NewOperatorNPoint(),
	}
	executioner := NewExecutionerStandard(0)

	ea := NewEA(GENERATION_SIZE, 0.1, 0, QUIT_AFTER, nil, nil, expValSetter, selector, executioner, operators...)
	testEA(ea)
	fmt.Println()
}
